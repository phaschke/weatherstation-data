package com.peterchaschke.weather.data.truncate;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

@Configuration
@EnableBatchProcessing
//@EnableJpaRepositories
@Import(DataSourceConfiguration.class)
public class BatchConfiguration {
	
	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	public DataSource dataSource;
	
	//@Autowired
	//public DataSource weatherDataSource;
	
	/*@Bean
	@ConfigurationProperties(prefix="spring.batch.datasource")
	public DataSource getBatchDataSource(){
	  return DataSourceBuilder.create().build();
	}
	
	@Override
	protected JobRepository createJobRepository() throws Exception {
	    JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
	    factory.setDataSource(dataSource);
	    factory.setTransactionManager(transactionManager);
	    factory.setIsolationLevelForCreate("ISOLATION_REPEATABLE_READ");
	    return factory.getObject();
	}*/
	
	
	//public DataSource weatherDataSource;
	
	/*@Autowired
	public BatchConfiguration(@Qualifier("weatherDb") final DataSource dataSource) {
		this.weatherDataSource = dataSource;
	}*/
	
	//@Autowired
	//public DataSource destinationDataSource;

	//@Autowired
	//public DataSource dataSource;
	
	
	//@Qualifier("weatherDb")
	//public DataSource weatherDataSource;

	//@Bean
	/*public DataSource dataSource() {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		//dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/jobs?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		//dataSource.setUsername("root");
		//dataSource.setPassword("rootpass");

		return dataSource;
	}*/
	
	/*@Bean
	//@ConfigurationProperties("app.datasource.weather")
	public DataSource weatherDataSource() throws SQLException {
		final DriverManagerDataSource weatherDataSource = new DriverManagerDataSource();
		
		//dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		weatherDataSource.setUrl("jdbc:mysql://localhost:3306/weather?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		//dataSource.setUsername("root");
		//dataSource.setPassword("rootpass");
		System.out.println(weatherDataSource.getUrl());

		return weatherDataSource;
	}*/
	

	/*@Bean
	public JdbcCursorItemReader<TruncateConfig> reader(){
		
		JdbcCursorItemReader<TruncateConfig> reader = new JdbcCursorItemReader<TruncateConfig>();
		
		System.out.println("in reader");

		reader.setDataSource(weatherDataSource);
		
		System.out.println("set data source");
		
		reader.setSql("SELECT id, mode, retention_days AS retentionDays FROM data_truncation");
		reader.setRowMapper(new UserRowMapper());
		
		System.out.println("row mapper");

		return reader;
	}

	public class UserRowMapper implements RowMapper<TruncateConfig>{
		
		public TruncateConfig mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			TruncateConfig truncateConfig = new TruncateConfig();
			truncateConfig.setId(rs.getInt("id"));
			truncateConfig.setMode(rs.getString("mode"));
			truncateConfig.setRetentionDays(rs.getInt("retentionDays"));

			return truncateConfig;
		}

	}

	@Bean
	public TruncateConfigItemProcessor processor(){
		return new TruncateConfigItemProcessor();
	}
	
	@Bean
	public TruncateDataProcessor truncateDataProcessor(){
		return new TruncateDataProcessor();
	}
	
	 @Bean
	 public NoOpItemWriter writer(){
		 NoOpItemWriter writer = new NoOpItemWriter();
	  
	  return writer;
	 }*/

	/*@Bean
	public Step step1() {
		return stepBuilderFactory.get("step1").<TruncateConfig, TruncateConfig> chunk(10)
				.reader(reader())
				.processor(processor())
				.writer(writer())
				.build();
	}
	
	public Step stepTruncate() {
		return stepBuilderFactory.get("stepTruncate").<TruncateConfig, TruncateConfig> chunk(10)
				.processor(truncateDataProcessor())
				.build();
	}

	@Bean
	public Job exportUserJob() {
		return jobBuilderFactory.get("exportUserJob")
				.incrementer(new RunIdIncrementer())
				.flow(step1())
				.end()
				.build();
	}*/
	 
	 @Bean
	    public ConfigReaderTasklet configReader() {
	      return new ConfigReaderTasklet();
	      //return new ConfigReader(truncateConfigRepository);
	    }
	    
	    @Bean
	    protected Step readConfig() {
	        return stepBuilderFactory
	          .get("readConfig")
	          .tasklet(configReader())
	          .build();
	    }
	    
	    @Bean
		public Job exportUserJob() {
			return jobBuilderFactory.get("exportUserJob")
					.incrementer(new RunIdIncrementer())
					.flow(readConfig())
					.end()
					.build();
		}


}
