package com.peterchaschke.weather.station;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StationSiteBridgeRepository extends JpaRepository<StationSiteBridge, Long> {
	
	public List<StationSiteBridge> findAllByStationIdAndSiteId(Long stationId, Long siteId);

}
