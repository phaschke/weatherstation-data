/*
   ESP8266 Weather Station
*/
#include "ESP8266WeatherStation.h"

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>

//-- Dallas One wire --
#include <OneWire.h>
#include <DallasTemperature.h>
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONEWIREBUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);
//--

//-- BME280 --
#include <Wire.h>
#include <Adafruit_BME280.h>
//#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme; // I2C
//--

const char* host = "api.peterchaschke.com";
const int httpsPort = 443;

WiFiClientSecure client;

void setup() {

  Serial.begin(115200);

  delay(3000);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSIDD, PASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {

    digitalWrite(LED_PIN, HIGH); // LED On
    delay(2000);
    digitalWrite(LED_PIN, LOW); // LED Off
    
  #if DEBUG
    Serial.println("Connecting to WiFi..");
  #endif
  }

  #if DEBUG
    Serial.println("Connected to the WiFi network");
  #endif

  digitalWrite(LED_PIN, HIGH); // LED Off

  // Use WiFiClientSecure class to create TLS connection
  client.setInsecure();

  //-- Dallas One wire --
  sensors.begin();
  //--

  //-- BME280 --
  bool statusBME;
  statusBME = bme.begin(0x76);  
  if (!statusBME) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
  //--
}

void loop() {

  char temperature[3];
  char humidity[3];

  sensors.requestTemperatures();
          
  //float temperatureC = sensors.getTempCByIndex(0);
  float temperatureF = sensors.getTempFByIndex(0);
  sprintf(temperature, "%0.1f\0", temperatureF);

  Serial.println(bme.readHumidity());

  StaticJsonDocument<256> requestJson;

  requestJson["stationId"] = 1;
  requestJson["siteId"] = 1;
  //requestJson["timestamp"] = "2020-06-03T23:01:05";
  requestJson["temp1"] = temperature;
  requestJson["humidity"] = bme.readHumidity();
  /*
  requestJson["pressure"] = 1032.0;
  requestJson["windDir"] = "N";
  requestJson["windSpeed"] = 5;
  requestJson["precip"] = 0.0;
  */

  char buffer[256];
  size_t n = serializeJson(requestJson, buffer);
  String request = String(buffer);

  httpsPost(request);

  delay(25000);

}

void httpsPost(String request) {

  //String url = "/weather/weather/stations/1/site/1/data";

  //String request = String(buffer);

  Serial.println(request);

  Serial.print("requesting URL: ");
  Serial.println((String)host);

  if (client.connect(host, httpsPort)) {

    Serial.println("here");
    
    client.println(String("POST ") + REQUEST_URL + " HTTP/1.1");
    client.println("Host: " + (String)host);
    client.println("User-Agent: ESP8266/1.0");
    client.println("Connection: close");
    client.println("Content-Type: application/json");
    client.print("Content-Length: ");
    client.println(request.length());
    client.print("API-USER: ");
    client.println(API_USER);
    client.print("API-KEY: ");
    client.println(API_KEY);
    client.println();
    client.println(request);
    delay(10);
    
    Serial.println("POST request sent");
  
    while (client.connected()) {
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        Serial.println("headers received");
        break;
      }
    }
  
    Serial.println("reply was:");
    Serial.println("==========");
    String line;
    while (client.available()) {
      line = client.readStringUntil('\n');  //Read Line by Line
      Serial.println(line); //Print response
    }
    Serial.println("==========");
    client.stop();
    Serial.println("closed connection");
  
  } else {
    Serial.println("Error connecting to webservice");
  }
}

String httpsGet(String url) {

  Serial.print("requesting URL: ");
  Serial.println((String)host+url);

  if (client.connect(host, httpsPort)) {
    client.println(String("GET ") + url + " HTTP/1.1");
    client.println("Host: " + (String)host);
    client.println("User-Agent: ESP8266/1.0");
    client.println("Connection: close");
    client.println();
    delay(10);
  
    Serial.println("GET request sent");
  
    while (client.connected()) {
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        Serial.println("headers received");
        break;
      }
    }
  
    Serial.println("reply was:");
    Serial.println("==========");
    String line;
    while (client.available()) {
      line = client.readStringUntil('\n');  //Read Line by Line
      Serial.println(line); //Print response
    }
    Serial.println("==========");
    client.stop();
    Serial.println("closed connection");
    
  } else {
    return "ERROR";
  }
  
}
