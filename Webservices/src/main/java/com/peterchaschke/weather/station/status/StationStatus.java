package com.peterchaschke.weather.station.status;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "station_status")
@Getter
@Setter
@NoArgsConstructor
public class StationStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "timestamp")
	private Instant timestamp;
	
	@Column(name = "sd")
	private int sdStatus;
	
	@Column(name = "rtc")
	private int rtcStatus;
	
	@Column(name = "bme")
	private int bmeStatus;
	
	@Column(name = "temp_1")
	private int temp1Status;
	
	@Column(name = "temp_2")
	private int temp2Status;
	
	@Column(name = "light")
	private int lightStatus;
	
	@Column(name = "rtc_batt_v")
	private float rtcBattVStatus;
	
	@Column(name = "weather_post")
	private int weatherPost;
	
	@Column(name = "reboot_time")
	private Instant rebootTime;
	
	@Column(name = "aux_power")
	private int auxPower;
	
	@Column(name = "aux_1")
	private int auxStatus1;
	
	@Column(name = "aux_2")
	private int auxStatus2;
	
	@Column(name = "aux_3")
	private int auxStatus3;
	
	public StationStatusResource toResource() {
		
		StationStatusResource resource = new StationStatusResource();
		
		resource.setId(this.id);
		resource.setStationId(this.stationId);
		if(this.timestamp != null) {
			resource.setTimestamp(this.timestamp.toString());
		}
		resource.setSdStatus(this.sdStatus);
		resource.setRtcStatus(this.rtcStatus);
		resource.setBmeStatus(this.bmeStatus);
		resource.setTemp1Status(this.temp1Status);
		resource.setTemp2Status(this.temp2Status);
		resource.setLightStatus(this.lightStatus);
		resource.setRtcBattVStatus(this.rtcBattVStatus);
		resource.setWeatherPost(this.weatherPost);
		if(this.rebootTime != null) {
			resource.setRebootTime(this.rebootTime.toString());
		}
		resource.setAuxPower(this.auxPower);
		resource.setAuxStatus1(this.auxStatus1);
		resource.setAuxStatus2(this.auxStatus3);
		resource.setAuxStatus2(this.auxStatus3);
		
		return resource;
	}
}
