package com.peterchaschke.weather.station;

import com.peterchaschke.weather.record.RecordModel;
import com.peterchaschke.weather.site.Site;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StationSummary {
	
	StationDetailsModel stationDetails;
	Site primarySite;
	RecordModel record;
	
}
