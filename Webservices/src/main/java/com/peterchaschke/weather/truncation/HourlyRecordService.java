package com.peterchaschke.weather.truncation;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.peterchaschke.weather.record.Record;

@Service
public class HourlyRecordService {
	
	@Autowired
	private final HourlyRecordRepository hourlyRecordRepository;
	
	HourlyRecordService(HourlyRecordRepository hourlyRecordRepository) {
		this.hourlyRecordRepository = hourlyRecordRepository;
	}
	
	public int getTotalHourlyRecordCount(Long stationId, Long siteId, Instant start, Instant end) {
		return hourlyRecordRepository.countAllByStationIdAndSiteIdAndTimestampBetween(stationId, siteId, start, end);
	}
	
	public List<Record> getRecordsFromHourlyData(Long stationId, Long siteId, Instant start, Instant end, Pageable pageable) {
		
		List<HourlyRecord> hourlyRecords = hourlyRecordRepository.findByStationIdAndSiteIdAndTimestampBetween(stationId, siteId, start, end, pageable);
		
		List<Record> records = new ArrayList<Record>();
		
		hourlyRecords.forEach(hourlyRecord -> {
			
			Record record = new Record();
			record.setStationId(hourlyRecord.getStationId());
			record.setSiteId(hourlyRecord.getSiteId());
			record.setTimestamp(hourlyRecord.getTimestamp());
			record.setTemp1(hourlyRecord.getTemp1()); 
			record.setTemp2(hourlyRecord.getTemp2());
			record.setHumidity(hourlyRecord.getHumidity());
			record.setPressure(hourlyRecord.getPressure());
			record.setWindDir(hourlyRecord.getWindDir());
			record.setWindSpeed(hourlyRecord.getWindSpeed());
			record.setWindGust(hourlyRecord.getWindGust());
			record.setWindMax(hourlyRecord.getWindMax());
			record.setPrecipitation(hourlyRecord.getPrecipitation());
			record.setDewPoint(hourlyRecord.getDewPoint());
			record.setHeatIndex(hourlyRecord.getHeatIndex());
			record.setWindChill(hourlyRecord.getWindChill());
			
			records.add(record);
		});
		
		return records;
	}

}
