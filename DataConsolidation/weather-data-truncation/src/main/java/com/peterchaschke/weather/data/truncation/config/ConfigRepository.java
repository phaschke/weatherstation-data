package com.peterchaschke.weather.data.truncation.config;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigRepository extends JpaRepository<ConfigEntry, Long> {

	List<ConfigEntry> findByActive(boolean flag);

}
