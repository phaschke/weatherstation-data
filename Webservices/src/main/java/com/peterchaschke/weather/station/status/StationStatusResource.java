package com.peterchaschke.weather.station.status;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.springframework.hateoas.RepresentationModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StationStatusResource extends RepresentationModel<StationStatusResource> {
	
	private Long id;
	private Long stationId;
	private String timestamp;
	private int sdStatus;
	private int rtcStatus;
	private int bmeStatus;
	private int temp1Status;
	private int temp2Status;
	private int lightStatus;
	private float rtcBattVStatus;
	private int weatherPost;
	private String rebootTime;
	private int auxPower;
	private int auxStatus1;
	private int auxStatus2;
	private int auxStatus3;
	
	
	public StationStatus toEntity() {
		
		StationStatus entity = new StationStatus();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d H:m:s");
		ZoneId zoneId = ZoneId.of("Etc/GMT");
		
		entity.setId(this.id);
		entity.setStationId(this.stationId);
		
		if(this.timestamp != null) {
			LocalDateTime ldt = LocalDateTime.parse(this.timestamp, formatter);
			Instant timestampInstant = ldt.atZone(zoneId).toInstant();
			entity.setTimestamp(timestampInstant);
		}
		
		entity.setSdStatus(this.sdStatus);
		entity.setRtcStatus(this.rtcStatus);
		entity.setBmeStatus(this.bmeStatus);
		entity.setTemp1Status(this.temp1Status);
		entity.setTemp2Status(this.temp2Status);
		entity.setLightStatus(this.lightStatus);
		entity.setRtcBattVStatus(this.rtcBattVStatus);
		entity.setWeatherPost(this.weatherPost);
		
		if(this.rebootTime != null) {
			LocalDateTime ldt = LocalDateTime.parse(this.rebootTime, formatter);
			Instant timestampInstant = ldt.atZone(zoneId).toInstant();
			entity.setRebootTime(timestampInstant);
		}
		
		entity.setAuxPower(this.auxPower);
		entity.setAuxStatus1(this.auxStatus1);
		entity.setAuxStatus2(this.auxStatus3);
		entity.setAuxStatus2(this.auxStatus3);
		
		return entity;
	}
}
