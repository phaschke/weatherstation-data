#ifndef _ESP8266WEATHERSTATION.H
#define _ESP8266WEATHERSTATION.H

// WiFi Connection
#define SSIDD ""
#define PASSWORD ""

// Station details
#define REQUEST_URL "/weather/weather/stations/{}/site/{}/data"
#define API_USER ""
#define API_KEY ""

// Device Settings
#define DEBUG 1

#define LED_PIN LED_BUILTIN

#define ONEWIREBUS 14

#endif //_ESP8266WEATHERSTATION.H
