package com.peterchaschke.weather.data.truncation.hourlyRecord;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.weather.data.truncation.records.RecordRepository;

@Service
@EnableTransactionManagement
public class HourlyRecordService {

	private static final Logger logger = LoggerFactory.getLogger(HourlyRecordService.class);

	@Autowired
	private final HourlyRecordRepository hourlyRecordRepository;
	private final RecordRepository recordRepository;

	HourlyRecordService(HourlyRecordRepository hourlyRecordRepository, RecordRepository recordRepository) {
		this.hourlyRecordRepository = hourlyRecordRepository;
		this.recordRepository = recordRepository;
	}

	@Transactional
	public void addOrUpdateThenDeleteRecords(Long stationId, Long siteId, List<HourlyRecord> hourlyRecords,
			LocalDateTime start) {

		LocalDateTime end = start.plusHours(23).plusMinutes(59).plusSeconds(59).plusNanos(999999999);

		logger.info(String.format("%o-%o: Begin writing of records for date %s.", stationId, siteId, start.toString()));

		for (HourlyRecord hourlyRecord : hourlyRecords) {

			List<HourlyRecord> matchingRecords = hourlyRecordRepository.findByStationIdAndSiteIdAndTimestamp(
					hourlyRecord.getStationId(), hourlyRecord.getSiteId(), hourlyRecord.getTimestamp());

			if (matchingRecords.size() > 0) {
				logger.warn("Hourly record already exists in database, overwriting...");
				HourlyRecord existingHourlyRecord = matchingRecords.get(0);
				existingHourlyRecord.setTemp1(hourlyRecord.getTemp1());
				existingHourlyRecord.setTemp2(hourlyRecord.getTemp2());
				existingHourlyRecord.setMaxTemp(hourlyRecord.getMaxTemp());
				existingHourlyRecord.setMinTemp(hourlyRecord.getMinTemp());
				existingHourlyRecord.setHumidity(hourlyRecord.getHumidity());
				existingHourlyRecord.setPressure(hourlyRecord.getPressure());
				existingHourlyRecord.setWindDir(hourlyRecord.getWindDir());
				existingHourlyRecord.setWindSpeed(hourlyRecord.getWindSpeed());
				existingHourlyRecord.setWindGust(hourlyRecord.getWindGust());
				existingHourlyRecord.setWindMax(hourlyRecord.getWindMax());
				existingHourlyRecord.setPrecip(hourlyRecord.getPrecip());
				existingHourlyRecord.setHeatIndex(hourlyRecord.getHeatIndex());
				existingHourlyRecord.setWindChill(hourlyRecord.getWindChill());

				hourlyRecordRepository.save(existingHourlyRecord);

			} else {
				hourlyRecordRepository.save(hourlyRecord);

			}

		}

		Instant startInstant = start.atZone(ZoneId.of("Etc/GMT")).toInstant();
		Instant endInstant = end.atZone(ZoneId.of("Etc/GMT")).toInstant();

		logger.info(String.format("%o-%o: Wrote %o records between %s and %s to hourly record table.", stationId,
				siteId, hourlyRecords.size(), startInstant.toString(), endInstant.toString()));

		// Remove successfully added or updated values
		recordRepository.deleteByStationIdAndSiteIdAndTimestampBetween(stationId, siteId, startInstant, endInstant);
		logger.info(String.format("%o-%o: Deleted %o records between %s and %s from record table.", stationId, siteId,
				hourlyRecords.size(), startInstant.toString(), endInstant.toString()));

	}

}
