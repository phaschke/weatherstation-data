package com.peterchaschke.weather.data.truncation.hourlyRecord;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HourlyRecordRepository extends JpaRepository<HourlyRecord, Long> {

	List<HourlyRecord> findByStationIdAndSiteIdAndTimestamp(Long stationId, Long siteId, Instant timestamp);
	
}
