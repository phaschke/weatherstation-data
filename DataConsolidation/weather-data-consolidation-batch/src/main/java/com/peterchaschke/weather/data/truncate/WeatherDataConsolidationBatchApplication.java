package com.peterchaschke.weather.data.truncate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherDataConsolidationBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherDataConsolidationBatchApplication.class, args);
	}

}
