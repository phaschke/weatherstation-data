package com.peterchaschke.weather.data.truncate;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peterchaschke.weather.data.truncate.config.TruncateConfigRepository;

@Component
public class ConfigReaderTasklet implements Tasklet {
	
	@Autowired
	private TruncateConfigRepository truncateConfigRepository;
	
	private List<TruncateConfig> configs;
	
	public void beforeStep(StepExecution stepExecution) {
		
		System.out.println("before");
		
	}
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		configs = truncateConfigRepository.findAll();
		//System.out.println(configs.size());
		System.out.println("execute");
		
		return RepeatStatus.FINISHED;
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		
		System.out.println("after");
		
		return ExitStatus.COMPLETED;
	}

	

}
