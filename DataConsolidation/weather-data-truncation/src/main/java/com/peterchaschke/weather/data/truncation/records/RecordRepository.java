package com.peterchaschke.weather.data.truncation.records;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long> {

	List<Record> findByTimestampBetween(Instant start, Instant end);

	List<Record> findByStationIdAndSiteIdAndTimestampBetweenOrderByTimestampAsc(Long stationId, Long siteId,
			Instant start, Instant end);

	void deleteByStationIdAndSiteIdAndTimestampBetween(Long stationId, Long siteId, Instant start, Instant end);

	long countByStationIdAndSiteIdAndTimestampBefore(Long stationId, Long siteId, Instant retentionDate);

	@Transactional
	void deleteByStationIdAndSiteIdAndTimestampBefore(Long stationId, Long siteId, Instant retentionDate);
}
