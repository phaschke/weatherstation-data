package com.peterchaschke.weather.site;

import java.util.List;

import org.springframework.stereotype.Service;

import com.peterchaschke.weather.EntityNotFoundException;

@Service
public class SiteService {
	
	public final SiteRepository siteRepository;
	
	SiteService(SiteRepository siteRepository) {
		this.siteRepository = siteRepository;
	}
	
	public List<Site> getActiveSitesByStation(Long stationId) {
		
		return siteRepository.findSitesByStationIdAndActive(stationId, 1);
	}
	
	public Site validateSiteExistsOrThrowException(Long siteId) throws EntityNotFoundException {
		
		return siteRepository.findById(siteId).orElseThrow(() -> new EntityNotFoundException(siteId));
	}
	
}
