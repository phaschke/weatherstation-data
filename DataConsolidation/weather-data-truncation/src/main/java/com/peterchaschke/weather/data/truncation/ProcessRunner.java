package com.peterchaschke.weather.data.truncation;

import java.time.Duration;
import java.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.peterchaschke.weather.data.truncation.config.ConfigService;

@Component
public class ProcessRunner implements ApplicationListener<ApplicationReadyEvent> {

	private static final Logger logger = LoggerFactory.getLogger(ProcessRunner.class);

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private ConfigService configService;

	/**
	 * This event is executed as late as conceivably possible to indicate that the
	 * application is ready to service requests.
	 */
	public void onApplicationEvent(final ApplicationReadyEvent event) {

		Instant start = Instant.now();
		logger.info(String.format("Beginning Truncation: %s.", start.toString()));

		configService.getConfigAndDoTruncation();

		Instant end = Instant.now();

		logger.info(String.format("Truncation Ended, Elapsed Time: %o seconds.", getDuration(start, end)));

		initiateShutdown(0);
	}

	private void initiateShutdown(final int returnCode) {
		SpringApplication.exit(appContext, () -> returnCode);
	}

	private long getDuration(Instant start, Instant end) {
		Duration timeElapsed = Duration.between(start, end);
		return timeElapsed.getSeconds();
	}

}
