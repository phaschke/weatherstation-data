package com.peterchaschke.weather.record;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RecordRepository extends JpaRepository<Record, Long> {
	
	Record findFirst1ByStationIdAndSiteIdOrderByTimestampDesc(Long stationId, Long siteId);
	int countAllByStationIdAndSiteIdAndTimestampBetween(Long stationId, Long siteId, Instant start, Instant end);
	List<Record> findAllByStationIdAndSiteIdAndTimestampBetween(Long stationId, Long siteId, Instant start, Instant end, Pageable pageable);
	
	@Query(value = "SELECT SUM(precipitation) FROM data WHERE DATE(timestamp) = :date AND station_id = :stationId AND site_id = :siteId", nativeQuery = true)
	public Double getPrecipToday(@Param("stationId") Long stationId, @Param("siteId") Long siteId, @Param("date") LocalDate date);
}