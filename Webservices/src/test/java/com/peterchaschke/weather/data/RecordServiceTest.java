package com.peterchaschke.weather.data;

import static org.junit.Assert.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.weather.record.Record;
import com.peterchaschke.weather.record.RecordService;
import com.peterchaschke.weather.site.Site;
import com.peterchaschke.weather.station.Station;

/*@SpringBootTest
@ActiveProfiles("test")
@Transactional
class RecordServiceTest {
	
	private Station station;
	private Site site;
	private Record record;
	
	@Autowired
	private RecordService recordService;
	
	@BeforeEach
	void init() {
		
		Station testStation = new Station();
		station.setId(new Long(0));
		station.setName("teststation");
		station.setDisplayName("Test Station");
		station.setTimezone("America/Denver");
		station.setDescription("Test description");
		station.setActive(1);
		station.setLocationDisplay("My Location");
		station.setElevation(1234);
		
		this.station = testStation;
		this.site = new Site(new Long(0), "site1", "Site 1", 1, true, true, true, true, "");
		this.record = new Record();
	}

	@Test
	@DisplayName("Calculate Dew Point")
	void testCalculateDewPoint() {
		
		record.setTemp1(70.5);
		record.setHumidity(45.3);
		
		Double expected = 48.3;
		
		try {
			Record savedRecord = recordService.addRecord(station, site, record);
			
			assertEquals(expected, savedRecord.getDewPoint());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("Calculate Heat Index with below valid temp")
	void testCalculateHeatIndexAsNull() {
		
		record.setTemp1(50.0);
		record.setHumidity(45.3);
		
		try {
			Record savedRecord = recordService.addRecord(station, site, record);
			
			assertNull(savedRecord.getHeatIndex());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("Calculate Heat Index")
	void testCalculateHeatIndex() {
		
		record.setTemp1(95.2);
		record.setHumidity(67.1);
		
		Double expected = 120.4;
		
		try {
			Record savedRecord = recordService.addRecord(station, site, record);
			
			assertEquals(expected, savedRecord.getHeatIndex());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}*/
