package com.peterchaschke.weather.station.status;

import javax.servlet.http.HttpServletRequest;

public interface StationStatusService {
	
	public StationStatusResource getStationStatus(Long stationId);
	public StationStatusResource updateStationStatus(HttpServletRequest request, Long stationId, StationStatusResource stationStatusResource);

}
