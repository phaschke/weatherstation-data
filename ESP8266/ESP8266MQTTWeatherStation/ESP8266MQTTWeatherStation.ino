/*
   ESP8266 Weather Station
*/
#include "ESP8266MQTTWeatherStation.h"

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

//-- Dallas One wire --
#include <OneWire.h>
#include <DallasTemperature.h>
//--
//-- BME280 --
#include <Wire.h>
#include <Adafruit_BME280.h>
//--

// Timer global variables
unsigned long delayStart = 0; // the time the delay started
unsigned long totalDelay;
bool delayRunning = false; // true if still waiting for delay to finish

// Global status variables
boolean useBME = true;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONEWIREBUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

//#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme; // I2C

WiFiClientSecure client;
WiFiClient wifiClient;
PubSubClient pubSubClient(wifiClient);

// ===============================================================
// Setup
// ===============================================================
void setup() {

  Serial.begin(115200);

  delay(3000);

  // Connect to WiFi
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSIDD, PASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {

    digitalWrite(LED_PIN, HIGH); // LED On
    delay(2000);
    digitalWrite(LED_PIN, LOW); // LED Off
    
  #if DEBUG
    Serial.println("Connecting to WiFi..");
  #endif
  }

  #if DEBUG
    Serial.println("Connected to the WiFi network");
  #endif

  // Configure MQTT server and callback
  pubSubClient.setServer(MQTT_SERVER, MQTT_PORT);
  pubSubClient.setCallback(callback);

  digitalWrite(LED_PIN, HIGH); // LED Off

  // Configure secure Wifi in insecure mode
  // (Still encrypts messages, but does not verify reciever)
  client.setInsecure();

  // One wire Initialization (For DS18B20)--
  sensors.begin();

  // BME280 Initialization
  bool statusBME;
  statusBME = bme.begin(0x76);  
  if (!statusBME) {
    #if DEBUG
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    #endif
    // Do not block on BME connection
    useBME = false;
  }

  // Start timer for sending weather data
  // Timer duration set in header file
  delayRunning = true;
  delayStart = millis();
  totalDelay = SEND_DURATION*1000;
}

// ===============================================================
// Loop
// ===============================================================
void loop() {

  // Connect to MQTT
  // Allow weather data to upload reguardless of MQTT connection
  if (!pubSubClient.connected()) {
    reconnect();
  }

  // Check timer for sending weather data
  // Check if there is a active running timer
  if (delayRunning && ((millis() - delayStart) >= totalDelay)) {

    #if DEBUG
      Serial.println("End of delay, sending weather data");
    #endif

    sendData();

    delayRunning = true;
    delayStart = millis();
    totalDelay = SEND_DURATION*1000;
  }

  delay(100);

  pubSubClient.loop();

}

// ===============================================================
// Handle connection to MQTT
// ===============================================================
void reconnect() {

  #if DEBUG
    Serial.println("Attempting MQTT connection...");
  #endif

  // If you do not want to use a username and password, change next line to
  if (pubSubClient.connect(DEVICE_NAME)) {
    //if (client.connect(DEVICE_NAME, mqtt_user, mqtt_password)) {

  #if DEBUG
    Serial.println("Connected to MQTT");
  #endif
    // Once connected, publish an announcement...
    //client.publish(IN_TOPIC, "Sonoff: booted");
    // ... and resubscribe
    pubSubClient.publish(OUT_TOPIC, "Sonoff Client Connected");
    pubSubClient.subscribe(IN_TOPIC);

  } else {
    
  #if DEBUG
    Serial.print("failed, rc=");
    Serial.print(pubSubClient.state());
    Serial.println(" try again in 5 seconds");
  #endif
    // Wait 5 seconds before retrying
    digitalWrite(LED_PIN, HIGH); // LED On
    delay(5000);
    digitalWrite(LED_PIN, LOW); // LED Off

  }
}

// ===============================================================
// Collect and send weather conditions
// ===============================================================
void sendData() {

  // Collect sensor data and build request
  char temperature[3];
  char humidity[3];
  
  sensors.requestTemperatures();
          
  float temperatureF = sensors.getTempFByIndex(0);
  sprintf(temperature, "%0.1f\0", temperatureF);

  //float humidityF = bme.readHumidity();
  //sprintf(humidity, "%0.1f\0", humidityF);

  StaticJsonDocument<256> requestJson;
  
  requestJson["stationId"] = STATION;
  requestJson["siteId"] = SITE;
  //requestJson["timestamp"] = "2020-06-03T23:01:05";
  requestJson["temp1"] = temperature;

  if(useBME) {
    requestJson["humidity"] = bme.readHumidity();
  }
  /*
  requestJson["pressure"] = 1032.0;
  requestJson["windDir"] = "N";
  requestJson["windSpeed"] = 5;
  requestJson["precip"] = 0.0;
  */

  // Convert JSON request into string
  char buffer[256];
  size_t n = serializeJson(requestJson, buffer);
  String request = String(buffer);
  
  #if DEBUG
    Serial.print("Requesting URL: ");
    Serial.println((String)HOST);
    Serial.print("Payload: ");
    Serial.println(request);
  #endif

  // Attempt connection to webservice and POST request
  if (client.connect(HOST, HTTPS_PORT)) {

    #if DEBUG
      Serial.println("Successfully connected to webservice, attempting POST");
    #endif
    
    client.println(String("POST ") + REQUEST_URL + " HTTP/1.1");
    client.println("Host: " + (String)HOST);
    client.println("User-Agent: ESP8266/1.0");
    client.println("Connection: close");
    client.println("Content-Type: application/json");
    client.print("Content-Length: ");
    client.println(request.length());
    client.print("API-USER: ");
    client.println(API_USER);
    client.print("API-KEY: ");
    client.println(API_KEY);
    client.println();
    client.println(request);
    delay(10);

    #if DEBUG
      Serial.println("POST request sent");
    #endif
  
    while (client.connected()) {
      String line = client.readStringUntil('\n');
      if (line == "\r") {
        #if DEBUG
          Serial.println("headers received");
        #endif
        
        break;
      }
    }

    #if DEBUG
      Serial.println("Reply was:");
    #endif
    String line;
    while (client.available()) {
      line = client.readStringUntil('\n');  //Read Line by Line
      #if DEBUG
        Serial.println(line); //Print response
      #endif
    }
    client.stop();
    #if DEBUG
      Serial.println("Closed webservice connection");
    #endif
  
  } else {
    
    #if DEBUG
      Serial.println("Error connecting to webservice");
    #endif
  }
}

// ===============================================================
// Code to be run when a MQTT message is recieved
// ===============================================================
void callback(char* topic, byte* payload, unsigned int length) {

  //client.publish(IN_TOPIC, "In callback");

  StaticJsonDocument<255> doc;

  char messageBuf[256];
  int toggleState;
  int duration;
  int i = 0;
  const size_t CAPACITY = JSON_ARRAY_SIZE(10);
  const size_t CAPACITY_OBJ = JSON_OBJECT_SIZE(2);

  char temperature[3];
  char humidity[3];

  // Limit length of input message
  if (length > 255) {
    length = 255;
  }

  // Convert byte array to char array for processing
  for (i = 0; i < length; i++) {
    messageBuf[i] = (char)payload[i];
  }
  messageBuf[i] = '\0';

   // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, messageBuf);

  // Test if parsing succeeds.
  if (error) {
    //TODO: Send back error
    #if DEBUG
      Serial.print(("deserializeJson() failed: "));
      Serial.println(error.c_str());
    #endif
    
    return;
  }

  const char* msgId = doc["msgId"];
  if(msgId) {

    // Allocate the memory for the output document
    StaticJsonDocument<512> outputDoc;

    #if DEBUG
      Serial.print("Recieved MQTT message with id: ");
      Serial.println(msgId);
    #endif
    outputDoc["resId"] = msgId;

    JsonArray jsonArray = doc["status"].as<JsonArray>();
    if(jsonArray) {
  
      // allocate the memory for the document
      StaticJsonDocument<CAPACITY> statusDoc;
      
      // create an empty array
      JsonArray statusArray = statusDoc.to<JsonArray>();
      
      // Create json array element?
      for(JsonVariant v : jsonArray) {
        
        const char* reqStatus = v.as<char*>();
        Serial.println(reqStatus);
        
        if(strcmp(reqStatus, "ping") == 0) {
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          object["ping"] = "pong";
          statusArray.add(object);

          #if DEBUG
            Serial.println("Ping... PONG!");
          #endif
          
        } else if(strcmp(reqStatus, "temp") == 0) {

          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          
          #if DEBUG
            Serial.println("Get status.");
          #endif
      
          sensors.requestTemperatures();
          
          float temperatureF = sensors.getTempFByIndex(0);
          sprintf(temperature, "%0.1f\0", temperatureF);

          #if DEBUG
            Serial.print("Current Temp: ");
            Serial.print(temperature);
            Serial.println("ºF");
          #endif
          
          object["temp"] = temperature;
          statusArray.add(object);
          
        } else if(strcmp(reqStatus, "humid") == 0) {
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();

          if(useBME) {
            float humidityF = bme.readHumidity();
            sprintf(humidity, "%0.1f\0", humidityF);
            object["humid"] = humidity;
            
          } else {
            object["humid"] = -1;
          }
          
          statusArray.add(object);
          
        } else {
          
          #if DEBUG
          Serial.println("Not valid state.");
          #endif
          
        }
      }
      outputDoc["status"] = statusArray;
    }

    char buffer[512];
    size_t n = serializeJson(outputDoc, buffer);
    pubSubClient.publish(OUT_TOPIC, buffer);

    #if DEBUG
    Serial.println(buffer);
    #endif
  
    return;
  }
  // No message id, do nothing
  return;
}
