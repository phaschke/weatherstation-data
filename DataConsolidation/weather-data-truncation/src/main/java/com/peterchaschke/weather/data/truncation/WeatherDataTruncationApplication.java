package com.peterchaschke.weather.data.truncation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherDataTruncationApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherDataTruncationApplication.class, args);
	}

}
