var host = "localhost";
var port = "8883";
var topic = "test/weather";

var client = new Paho.MQTT.Client(host, Number(port), "clientId");
var uid = 0;

function sendMessage() {

  // set callback handlers
  client.onConnectionLost = onConnectionLost;
  client.onMessageArrived = onMessageArrived;

  // connect the client
  client.connect({onSuccess:onConnect});

}


// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  //console.log("onConnect");
  client.subscribe(topic);

  uid = Math.floor(Math.random()*90000) + 10000;
  var request = {
    "msgId":uid.toString()
  };
  //console.log(JSON.stringify(request));

  //message = new Paho.MQTT.Message("Hello");
  message = new Paho.MQTT.Message(JSON.stringify(request));
  message.destinationName = topic;
  client.send(message);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  //console.log("onMessageArrived:"+message.payloadString);
  var response = JSON.parse(message.payloadString);

  if(response.resId) {
    var response = JSON.parse(message.payloadString);

    if(uid.toString() === response.resId.toString()) {
      if(response.temp) {
        //document.getElementById("temp").innerHTML = response.temp+"&#176;F";
        var roundedTemp = Math.round(response.temp * 10) / 10;
        document.getElementById("temp").innerHTML = roundedTemp+"&#176;F";
      }
      if(response.humid && response.temp) {

        var t = parseInt(response.temp);
        var rh = parseInt(response.humid);
        // Calculate dew point
        //http://bmcnoldy.rsmas.miami.edu/Humidity.html
			  //243.04*(LN(RH/100)+((17.625*T)/(243.04+T)))/(17.625-LN(RH/100)-((17.625*T)/(243.04+T)))
        // Equation expects temp in C
        t = (t-32)*5/9;
        var dewPoint = 243.04*(Math.log(rh/100)+((17.625*t)/(243.04+t)))/(17.625-Math.log(rh/100)-((17.625*t)/(243.04+t)));
        // Convert back to F
        dewPoint = (dewPoint*9/5)+32;
        var roundedDewPoint = Math.round(dewPoint * 10) / 10;

        document.getElementById("dew-point").innerHTML = roundedDewPoint+"&#176;F";
      }
      if(response.windS) {
        var roundedwindS = Math.round(response.windS * 10) / 10;
        document.getElementById("wind-speed").innerHTML = roundedwindS+"MPH";
      }
      if(response.windD) {
        document.getElementById("wind-dir").innerHTML = response.windD;
      }
    }

    client.disconnect();
    //console.log("disconnect");

  }

}
