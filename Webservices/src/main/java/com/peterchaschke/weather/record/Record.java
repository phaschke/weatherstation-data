package com.peterchaschke.weather.record;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "data")
public class Record {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "site_id")
	private Long siteId;
	
	private Instant timestamp;
	private Double temp1;
	private Double temp2;
	private Double humidity;
	private Double pressure;
	
	@Column(name = "wind_dir")
	private String windDir;
	
	@Column(name = "wind_speed")
	private Double windSpeed;
	
	@Column(name = "wind_gust")
	private Double windGust;
	
	@Column(name = "wind_max")
	private Double windMax;
	
	@Column(name = "precipitation")
	private Double precipitation;

	@Column(name = "dew_point")
	private Double dewPoint;
	
	@Column(name = "heat_index")
	private Double heatIndex;
	
	@Column(name = "wind_chill")
	private Double windChill;
	
	@Transient
	private Double precipToday;

	public Record() {}
	
	Record(Double temp1, Double temp2, Double humidity, Double pressure, String windDir, Double windSpeed, Double precipitation) {
		this.temp1 = temp1;
		this.temp2 = temp2;
		this.humidity = humidity;
		this.pressure = pressure;
		this.windDir = windDir;
		this.windSpeed = windSpeed;
		this.precipitation = precipitation;
	}

	public Long getId(){
		return id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
	
	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Double getTemp1() {
		return temp1;
	}

	public void setTemp1(Double temp1) {
		this.temp1 = temp1;
	}
	
	public Double getTemp2() {
		return temp2;
	}

	public void setTemp2(Double temp2) {
		this.temp2 = temp2;
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}
	
	public String getWindDir() {
		return windDir;
	}
	
	public void setWindDir(String windDir) {
		this.windDir = windDir;
	}
	
	public Double getWindSpeed() {
		return windSpeed;
	}
	
	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}
	
	public Double getWindGust() {
		return windGust;
	}
	
	public void setWindGust(Double windGust) {
		this.windGust = windGust;
	}
	
	public Double getWindMax() {
		return windMax;
	}
	
	public void setWindMax(Double windMax) {
		this.windMax = windMax;
	}
	
	public Double getPrecipitation() {
		return precipitation;
	}
	
	public void setPrecipitation(Double precipitation) {
		this.precipitation = precipitation;
	}
	
	public Double getDewPoint() {
		return dewPoint;
	}
	
	public void setDewPoint(Double dewPoint) {
		this.dewPoint = dewPoint;
	}
	
	public Double getHeatIndex() {
		return heatIndex;
	}
	
	public void setHeatIndex(Double heatIndex) {
		this.heatIndex = heatIndex;
	}
	
	public Double getWindChill() {
		return windChill;
	}
	
	public void setWindChill(Double windChill) {
		this.windChill = windChill;
	}
	
	public Double getPrecipToday() {
		return precipToday;
	}
	
	public void setPrecipToday(Double precipToday) {
		this.precipToday = precipToday;
	}
	
	public RecordModel toModel() {
		
		RecordModel recordModel = new RecordModel();
		
		recordModel.setStationId(this.getStationId());
		recordModel.setSiteId(this.getSiteId());
		recordModel.setTimestamp(this.getTimestamp());
		recordModel.setTemp1(this.getTemp1());
		recordModel.setTemp2(this.getTemp2());
		recordModel.setHumidity(this.getHumidity());
		recordModel.setPressure(this.getPressure());
		recordModel.setDewPoint(this.getDewPoint());
		recordModel.setHeatIndex(this.getHeatIndex());
		recordModel.setWindChill(this.getWindChill());
		recordModel.setWindDir(this.getWindDir());
		recordModel.setWindSpeed(this.getWindSpeed());
		recordModel.setWindGust(this.getWindGust());
		recordModel.setWindMax(this.getWindMax());
		recordModel.setPrecipitation(this.getPrecipitation());
		recordModel.setPrecipToday(this.getPrecipToday());
		
		return recordModel;
	}
	
	public void printRecord() {
		
		System.out.println(getTimestamp());
		System.out.println(getTemp1());
		System.out.println(getTemp2());
		System.out.println(getHumidity());
		System.out.println(getPressure());
		System.out.println(getPrecipitation());
		System.out.println(getWindSpeed());
		System.out.println(getWindDir());
		System.out.println(getHeatIndex());
		System.out.println(getWindChill());
		System.out.println(getDewPoint());
	}

}