package com.peterchaschke.weather;

public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7329609422480662638L;

	public EntityNotFoundException(Long id) {
		super("Resource not found with parameters {" + id + "}");
	}

}