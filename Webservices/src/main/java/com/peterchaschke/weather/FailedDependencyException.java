package com.peterchaschke.weather;

public class FailedDependencyException extends RuntimeException {
	
	private static final long serialVersionUID = 5929328046408161696L;
	
	private FieldError fieldError;
	
	public FailedDependencyException(String field, Object rejectedValue, String message) {
		this.fieldError = new FieldError(field, rejectedValue, message);
	}
	
	public String getMessage() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Failed Dependency.\n");
		sb.append("Field: ");
		sb.append(fieldError.getFieldName());
		sb.append("\n");
		sb.append("Rejected Value: ");
		sb.append(fieldError.getRejectedValue());
		sb.append("\n");
		sb.append("Message: ");
		sb.append(fieldError.getMessage());
		sb.append("\n");
		
		return sb.toString();
	}
}
