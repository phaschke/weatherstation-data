package com.peterchaschke.weather.station.status;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StationStatusRepository extends JpaRepository<StationStatus, Long> {
	
	public Optional<StationStatus> findOneByStationId(Long stationId);
}
