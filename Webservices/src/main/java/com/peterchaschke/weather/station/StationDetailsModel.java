package com.peterchaschke.weather.station;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.weather.site.Site;

@JsonRootName(value = "data")
public class StationDetailsModel {

	@JsonProperty("station")
	Station station;
	@JsonProperty("sites")
	List<Site> sites;

	public void setStation(Station station) {
		this.station = station;
	}

	public Station getStation() {
		return station;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public List<Site> getSites() {
		return sites;
	}

}
