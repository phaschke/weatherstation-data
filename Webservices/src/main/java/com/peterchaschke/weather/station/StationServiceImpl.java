package com.peterchaschke.weather.station;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.weather.EntityNotFoundException;
import com.peterchaschke.weather.record.Record;
import com.peterchaschke.weather.record.RecordRepository;
import com.peterchaschke.weather.site.Site;
import com.peterchaschke.weather.site.SiteRepository;

@Service
public class StationServiceImpl implements StationService {
	
	@Autowired
	private StationRepository stationRepository;
	
	@Autowired
	private StationSiteBridgeRepository stationSiteBridgeRepository;
	
	@Autowired
	private SiteRepository siteRepository;
	
	@Autowired
	private RecordRepository recordRepository;
	
	public List<Station> getAllStations() {
		
		return stationRepository.findAll();
	}
	
	public List<StationSummary> getAllStationSummaries() {
		
		List<StationSummary> summaries = new ArrayList<StationSummary>();
		
		List<Station> stations = stationRepository.findAllByActive(1);
		stations.forEach(station -> {
			
			StationSummary summary = new StationSummary();
			
			List<Site> sites = siteRepository.findSitesByStationIdAndActive(station.getId(), 1);
			
			StationDetailsModel stationDetailsModel = new StationDetailsModel();
			stationDetailsModel.setStation(station);
			stationDetailsModel.setSites(sites);
			
			summary.setStationDetails(stationDetailsModel);
			
			sites.forEach(site -> {
				if(site.isPrimary()) {
					Record record = recordRepository.findFirst1ByStationIdAndSiteIdOrderByTimestampDesc(station.getId(), site.getId());
					summary.setPrimarySite(site);
					if(record != null) {
						summary.setRecord(record.toModel());
					}
				}
			});
			
			summaries.add(summary);
		});
		
		return summaries; 
	}
	
	public List<StationModel> getAllActiveStations() {
		
		List<Station> stations = stationRepository.findAllByActive(1);
		
		return stations.stream().map(Station::toModel).collect(Collectors.toList());
	}
	
	public Optional<Station> findStationById(Long stationId) {
		
		return stationRepository.findById(stationId);
	}
	
	public StationDetailsModel getStationDetailsModel(Long stationId) throws Exception {
		
		Optional<Station> station = findStationById(stationId);
		if(!station.isPresent()) throw new Exception("Requested station does not exist");
		
		//List<Site> sites = siteService.getActiveSitesByStation(stationId);
		List<Site> sites = siteRepository.findSitesByStationIdAndActive(stationId, 1);
		
		StationDetailsModel stationDetailsModel = new StationDetailsModel();
		stationDetailsModel.setStation(station.get());
		stationDetailsModel.setSites(sites);
		
		return stationDetailsModel;
	}
	
	public Station validateStationExistsOrThrowException(Long stationId) throws EntityNotFoundException {
		
		return findStationById(stationId).orElseThrow(() -> new EntityNotFoundException(stationId));
	}
	
	public void validateStationWithSite(Long stationId, Long siteId) throws Exception {
		
		if (stationSiteBridgeRepository.findAllByStationIdAndSiteId(stationId, siteId).size() <= 0) {
			
			throw new Exception("Station and site ID mismatch");
		}
		
		return;
		
	}
}
