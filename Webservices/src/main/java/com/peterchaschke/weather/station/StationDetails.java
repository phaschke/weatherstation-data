package com.peterchaschke.weather.station;

import java.util.List;

import com.peterchaschke.weather.site.Site;

public class StationDetails {

	Station station;
	List<Site> sites;

	public StationDetails(Station station, List<Site> sites) {
		this.station = station;
		this.sites = sites;
	}

	public StationDetails() {
	};

	public void setStation(Station station) {
		this.station = station;
	}

	public Station getStation() {
		return station;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public List<Site> getSites() {
		return sites;
	}

	public StationDetailsModel toModel() {

		StationDetailsModel stationDetailsModel = new StationDetailsModel();

		stationDetailsModel.setStation(this.getStation());
		stationDetailsModel.setSites(this.getSites());

		return stationDetailsModel;
	}

}
