package com.peterchaschke.weather.site;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SiteRepository extends JpaRepository<Site, Long> {
	
	@Query(value = "SELECT a.* FROM site AS a INNER JOIN station_site AS b on a.id = b.site_id WHERE b.station_id=:stationId AND a.active=:active ORDER BY a.list_order ASC", nativeQuery = true)
	public List<Site> findSitesByStationIdAndActive(Long stationId, int active);

}
