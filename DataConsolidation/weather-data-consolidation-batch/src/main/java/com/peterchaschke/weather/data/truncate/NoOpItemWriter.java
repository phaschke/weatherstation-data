package com.peterchaschke.weather.data.truncate;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

public class NoOpItemWriter implements ItemWriter<Object> {

	public void write(List<? extends Object> items) throws Exception {
	}

}
