package com.peterchaschke.weather.record;

public enum DataSource {
	RECENT,
	TRUNCATED,
	BOTH
}
