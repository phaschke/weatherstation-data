package com.peterchaschke.weather.record;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class IncomingRecord {
	
	private Long stationId;
	private Long siteId;
	private String timestamp;
	private Double temp1;
	private Double temp2;
	private Double humidity;
	private Double pressure;
	private String windDir;
	private Double windSpeed;
	private Double windGust;
	private Double windMax;
	private Double precip;
	
	public Long getStationId() {
		return stationId;
	}
	
	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
	
	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public Double getTemp1() {
		return temp1;
	}
	public void setTemp1(Double temp1) {
		this.temp1 = temp1;
	}
	public Double getTemp2() {
		return temp2;
	}
	public void setTemp2(Double temp2) {
		this.temp2 = temp2;
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public String getWindDir() {
		return windDir;
	}

	public void setWindDir(String windDir) {
		this.windDir = windDir;
	}

	public Double getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}
	
	public Double getWindGust() {
		return windGust;
	}

	public void setWindGust(Double windGust) {
		this.windGust = windGust;
	}
	
	public Double getWindMax() {
		return windMax;
	}

	public void setWindMax(Double windMax) {
		this.windMax = windMax;
	}

	public Double getPrecip() {
		return precip;
	}

	public void setPrecip(Double precip) {
		this.precip = precip;
	}
	
	public Record toRecord() {
		
		Record record = new Record();
		
		record.setStationId(this.getStationId());
		record.setSiteId(this.getSiteId());
		
		if(this.timestamp != null) {
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d H:m:s");
			LocalDateTime ldt = LocalDateTime.parse(this.timestamp, formatter);

			ZoneId zoneId = ZoneId.of("Etc/GMT");
			Instant timestamp = ldt.atZone(zoneId).toInstant();
			record.setTimestamp(timestamp);
		}
		
		record.setTemp1(this.getTemp1());
		record.setTemp2(this.getTemp2());
		record.setHumidity(this.getHumidity());
		record.setPressure(this.getPressure());
		record.setWindDir(this.getWindDir());
		record.setWindSpeed(this.getWindSpeed());
		record.setWindGust(this.getWindGust());
		record.setWindMax(this.getWindMax());
		record.setPrecipitation(this.getPrecip());		
		
		return record;
	}

}
