-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 18, 2021 at 10:50 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `weather`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int NOT NULL,
  `station_id` int NOT NULL,
  `site_id` int NOT NULL,
  `timestamp` datetime NOT NULL,
  `temp1` decimal(4,1) DEFAULT NULL,
  `temp2` decimal(4,1) DEFAULT NULL,
  `humidity` decimal(3,1) DEFAULT NULL,
  `pressure` decimal(5,1) DEFAULT NULL,
  `wind_dir` varchar(3) DEFAULT NULL,
  `wind_speed` decimal(4,1) DEFAULT NULL,
  `wind_gust` decimal(4,1) DEFAULT NULL,
  `precipitation` decimal(4,2) DEFAULT NULL,
  `dew_point` decimal(4,1) DEFAULT NULL,
  `heat_index` decimal(4,1) DEFAULT NULL,
  `wind_chill` decimal(4,1) DEFAULT NULL,
  `wind_max` decimal(4,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_truncation`
--

CREATE TABLE `data_truncation` (
  `id` int NOT NULL,
  `station_id` int NOT NULL,
  `site_id` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `mode` enum('consolidate','delete') NOT NULL,
  `retention_days` int NOT NULL,
  `last_consolidated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hourly_data`
--

CREATE TABLE `hourly_data` (
  `id` int NOT NULL,
  `station_id` int NOT NULL,
  `site_id` int NOT NULL,
  `timestamp` datetime NOT NULL,
  `temp1` decimal(4,1) DEFAULT NULL,
  `temp2` decimal(4,1) DEFAULT NULL,
  `max_temp` decimal(4,1) DEFAULT NULL,
  `min_temp` decimal(4,1) DEFAULT NULL,
  `humidity` decimal(3,1) DEFAULT NULL,
  `pressure` decimal(5,1) DEFAULT NULL,
  `wind_dir` varchar(3) DEFAULT NULL,
  `wind_speed` decimal(4,1) DEFAULT NULL,
  `wind_gust` decimal(4,1) DEFAULT NULL,
  `precipitation` decimal(4,2) DEFAULT NULL,
  `dew_point` decimal(4,1) DEFAULT NULL,
  `heat_index` decimal(4,1) DEFAULT NULL,
  `wind_chill` decimal(4,1) DEFAULT NULL,
  `wind_max` decimal(4,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `id` int NOT NULL,
  `name` varchar(31) NOT NULL,
  `title` varchar(31) NOT NULL,
  `list_order` int NOT NULL DEFAULT '0',
  `active` int NOT NULL DEFAULT '1',
  `calc_dewpoint` tinyint(1) NOT NULL DEFAULT '0',
  `calc_heatindex` tinyint(1) NOT NULL DEFAULT '0',
  `calc_wind_chill` tinyint NOT NULL DEFAULT '0',
  `daylight_savings_time_active` tinyint NOT NULL DEFAULT '0',
  `view_settings` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE `station` (
  `id` int NOT NULL,
  `name` varchar(63) NOT NULL,
  `display_name` varchar(63) NOT NULL,
  `timezone` varchar(20) NOT NULL,
  `list_order` int NOT NULL DEFAULT '0',
  `active` int NOT NULL DEFAULT '1',
  `location_display` varchar(63) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `elevation` int DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `station_site`
--

CREATE TABLE `station_site` (
  `id` int NOT NULL,
  `station_id` int NOT NULL,
  `site_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `station_id` int NOT NULL,
  `name` varchar(31) NOT NULL,
  `active` int NOT NULL DEFAULT '0',
  `api_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_station$data` (`station_id`),
  ADD KEY `fk_site$data` (`site_id`);

--
-- Indexes for table `data_truncation`
--
ALTER TABLE `data_truncation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hourly_data`
--
ALTER TABLE `hourly_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_station$data` (`station_id`),
  ADD KEY `fk_site$data` (`site_id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `station_site`
--
ALTER TABLE `station_site`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_station_bridge` (`station_id`) USING BTREE,
  ADD KEY `ix_site_bridge` (`site_id`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `ix_users$stations` (`station_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_truncation`
--
ALTER TABLE `data_truncation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hourly_data`
--
ALTER TABLE `hourly_data`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `station`
--
ALTER TABLE `station`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `station_site`
--
ALTER TABLE `station_site`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data`
--
ALTER TABLE `data`
  ADD CONSTRAINT `fk_site$data` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_station$data` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `station_site`
--
ALTER TABLE `station_site`
  ADD CONSTRAINT `fk_site_bridge` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_station_bridge` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_users$stations` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
