package com.peterchaschke.weather.station;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "station")
@Getter
@Setter
@NoArgsConstructor
public class Station {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	private String name;

	@Column(name = "display_name")
	private String displayName;

	private String timezone;

	private String description;

	private int active;

	@Column(name = "location_display")
	private String locationDisplay;

	private int elevation;
	
	@Column(name = "view_settings")
	private String viewSettings;

	public StationModel toModel() {

		StationModel stationModel = new StationModel();

		stationModel.setId(this.getId());
		stationModel.setName(this.getName());
		stationModel.setDisplayName(this.getDisplayName());
		stationModel.setTimezone(this.getTimezone());
		stationModel.setDescription(this.getDescription());
		stationModel.setActive(this.getActive());
		stationModel.setLocationDisplay(this.getLocationDisplay());
		stationModel.setElevation(this.getElevation());
		stationModel.setViewSettings(this.getViewSettings());

		return stationModel;
	}

}