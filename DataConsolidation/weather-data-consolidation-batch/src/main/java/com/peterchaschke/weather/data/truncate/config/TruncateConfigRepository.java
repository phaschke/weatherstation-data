package com.peterchaschke.weather.data.truncate.config;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.peterchaschke.weather.data.truncate.TruncateConfig;

@Repository
public interface TruncateConfigRepository extends JpaRepository<TruncateConfig, Long> {

}
