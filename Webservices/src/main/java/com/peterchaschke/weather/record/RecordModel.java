package com.peterchaschke.weather.record;

import java.time.Instant;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "record")
@Relation(collectionRelation = "records")
public class RecordModel extends RepresentationModel<RecordModel>{
	
	private Long id;
	private Long stationId;
	private Long siteId;
	private Instant timestamp;
	private Double temp1;
	private Double temp2;
	private Double humidity;
	private Double pressure;
	private String windDir;
	private Double windSpeed;
	private Double windGust;
	private Double windMax;
	private Double precipitation;
	private Double dewPoint;
	private Double heatIndex;
	private Double windChill;
	private Double precipToday;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getStationId() {
		return stationId;
	}
	
	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
	
	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	
	public Instant getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}
	public Double getTemp1() {
		return temp1;
	}
	public void setTemp1(Double temp1) {
		this.temp1 = temp1;
	}
	public Double getTemp2() {
		return temp2;
	}
	public void setTemp2(Double temp2) {
		this.temp2 = temp2;
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public String getWindDir() {
		return windDir;
	}

	public void setWindDir(String windDir) {
		this.windDir = windDir;
	}

	public Double getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}
	
	public Double getWindGust() {
		return windGust;
	}

	public void setWindGust(Double windGust) {
		this.windGust = windGust;
	}
	
	public Double getWindMax() {
		return windMax;
	}

	public void setWindMax(Double windMax) {
		this.windMax = windMax;
	}

	public Double getPrecipitation() {
		return precipitation;
	}

	public void setPrecipitation(Double precipitation) {
		this.precipitation = precipitation;
	}
	
	public Double getDewPoint() {
		return dewPoint;
	}
	
	public void setDewPoint(Double dewPoint) {
		this.dewPoint = dewPoint;
	}
	
	public Double getHeatIndex() {
		return heatIndex;
	}
	
	public void setHeatIndex(Double heatIndex) {
		this.heatIndex = heatIndex;
	}
	
	public Double getWindChill() {
		return windChill;
	}
	
	public void setWindChill(Double windChill) {
		this.windChill = windChill;
	}
	
	public Double getPrecipToday() {
		return precipToday;
	}
	
	public void setPrecipToday(Double precipToday) {
		this.precipToday = precipToday;
	}

}
