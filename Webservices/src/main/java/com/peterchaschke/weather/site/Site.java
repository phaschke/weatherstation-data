package com.peterchaschke.weather.site;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "site")
@Getter
@Setter
@NoArgsConstructor
public class Site {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	private String name;
	private String title;
	
	@Column(name = "is_primary")
	private boolean isPrimary;
	
	private int active;
	
	@Column(name = "calc_dewpoint")
	private boolean calcDewPoint;
	
	@Column(name = "calc_heatindex")
	private boolean calcHeatIndex;
	
	@Column(name = "calc_wind_chill")
	private boolean calcWindChill;
	
	@Column(name = "daylight_savings_time_active")
	private boolean daylightSavingsTimeActive;
	
	@Column(name = "view_settings")
	private String viewSettings;
	
	public Site(Long id, String name, String title, int active, boolean calcDewPoint, boolean calcHeatIndex, boolean calcWindChill, boolean daylightSavingsTimeActive, String viewSettings) {
		this.id = id;
		this.name = name;
		this.title = title;
		this.active = active;
		this.calcDewPoint = calcDewPoint;
		this.calcHeatIndex = calcHeatIndex;
		this.calcWindChill = calcWindChill;
		this.daylightSavingsTimeActive = daylightSavingsTimeActive;
		this.viewSettings = viewSettings;
	}
	
	public SiteModel toModel() {
		
		SiteModel siteModel = new SiteModel();
		
		siteModel.setId(this.getId());
		siteModel.setName(this.getName());
		siteModel.setTitle(this.getTitle());
		siteModel.setActive(this.getActive());
		siteModel.setViewSettings(this.getViewSettings());
		
		return siteModel;

	}

}
