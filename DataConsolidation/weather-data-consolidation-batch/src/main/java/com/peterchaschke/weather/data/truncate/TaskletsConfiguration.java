package com.peterchaschke.weather.data.truncate;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

//@Configuration
//@EnableBatchProcessing
//@Import(DataSourceConfiguration.class)
/*public class TaskletsConfiguration {

	@Autowired 
    private JobBuilderFactory jobs;
 
    @Autowired 
    private StepBuilderFactory steps;
    
    @Autowired
	public DataSource dataSource;*/
    
    /*private TruncateConfigRepository truncateConfigRepository;
    
    @Autowired
    public TaskletsConfiguration(TruncateConfigRepository truncateConfigRepository) {
    	this.truncateConfigRepository = truncateConfigRepository;
    }
    
    @Bean(name="entityManagerFactory")
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

        return sessionFactory;
    }*/ 
    
    /*@Bean
    public ConfigReader configReader() {
      return new ConfigReader();
      //return new ConfigReader(truncateConfigRepository);
    }
    
    @Bean
    protected Step readConfig() {
        return steps
          .get("readConfig")
          .tasklet(configReader())
          .build();
    }
    
    @Bean
	public Job exportUserJob() {
		return jobs.get("exportUserJob")
				.incrementer(new RunIdIncrementer())
				.flow(readConfig())
				.end()
				.build();
	}*/

//}
