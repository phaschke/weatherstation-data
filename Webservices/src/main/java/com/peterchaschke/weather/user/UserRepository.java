package com.peterchaschke.weather.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByNameAndActive(String name, int active);

}
