package com.peterchaschke.weather.truncation;

import java.time.Instant;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HourlyRecordRepository extends JpaRepository<HourlyRecord, Long> {

	List<HourlyRecord> findByStationIdAndSiteIdAndTimestampBetween(Long stationId, Long siteId, Instant start, Instant end, Pageable pageable);
	int countAllByStationIdAndSiteIdAndTimestampBetween(Long stationId, Long siteId, Instant start, Instant end);
}
