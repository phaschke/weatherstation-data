package com.peterchaschke.weather.data.truncation.actions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.peterchaschke.weather.data.truncation.config.ConfigEntry;
import com.peterchaschke.weather.data.truncation.config.ConfigRepository;
import com.peterchaschke.weather.data.truncation.hourlyRecord.HourlyRecord;
import com.peterchaschke.weather.data.truncation.hourlyRecord.HourlyRecordService;
import com.peterchaschke.weather.data.truncation.records.Record;
import com.peterchaschke.weather.data.truncation.records.RecordRepository;

public class ConsolidateAction extends TruncationAction {

	private static final Logger logger = LoggerFactory.getLogger(ConsolidateAction.class);

	private final RecordRepository recordRepository;
	private final ConfigRepository configRepository;
	private final HourlyRecordService hourlyRecordService;

	public ConsolidateAction(RecordRepository recordRepository, ConfigRepository configRepository,
			HourlyRecordService hourlyRecordService) {
		this.recordRepository = recordRepository;
		this.configRepository = configRepository;
		this.hourlyRecordService = hourlyRecordService;
	}

	@Override
	public void doTruncation(ConfigEntry config) {

		ZoneId z = ZoneId.of("Etc/GMT");
		LocalDate today = LocalDate.now(z);
		LocalDate dateToRetention = today.minusDays(config.getRetentionDays());

		LocalDate lastTruncatedDate = config.getLastConsolidatedDate();

		while (lastTruncatedDate.isBefore(dateToRetention)) {

			LocalDateTime start = lastTruncatedDate.atStartOfDay();
			List<HourlyRecord> hourlyRecords = getHourlyRecords(config.getStationId(), config.getSiteId(), start);

			if (hourlyRecords.size() > 0) {
				hourlyRecordService.addOrUpdateThenDeleteRecords(config.getStationId(), config.getSiteId(),
						hourlyRecords, start);
			} else {
				logger.info(String.format("%o-%o: No hourly records for day %s", config.getStationId(),
						config.getSiteId(), lastTruncatedDate.toString()));
			}

			lastTruncatedDate = lastTruncatedDate.plusDays(1);

			config.setLastConsolidatedDate(lastTruncatedDate);
			configRepository.save(config);
			logger.info(String.format("%o-%o: Set last consolidation date to %s.", config.getStationId(),
					config.getSiteId(), lastTruncatedDate.toString()));
		}

		// System.out.println(lastTruncatedDate.toString());
	}

	private List<HourlyRecord> getHourlyRecords(Long stationId, Long siteId, LocalDateTime start) {

		List<HourlyRecord> hourlyRecords = new ArrayList<HourlyRecord>();

		LocalDateTime end = start.plusMinutes(59).plusSeconds(59).plusNanos(999999999);

		for (int i = 0; i <= 23; i++) {

			// System.out.println(String.format(stationId+"-"+siteId+": start %s, end %s",
			// start.toString(), end.toString()));

			List<Record> records = recordRepository.findByStationIdAndSiteIdAndTimestampBetweenOrderByTimestampAsc(
					stationId, siteId, start.atZone(ZoneId.of("Etc/GMT")).toInstant(),
					end.atZone(ZoneId.of("Etc/GMT")).toInstant());

			// If no records within the hour, skip truncation of the hour
			if (records.size() > 0) {
				hourlyRecords.add(truncateHour(stationId, siteId, records, start));
			}

			start = start.plusHours(1);
			end = end.plusHours(1);
		}

		return hourlyRecords;
	}

	private HourlyRecord truncateHour(Long stationId, Long siteId, List<Record> records, LocalDateTime start) {

		// Record list is by timestamp decreasing
		HourlyRecord hourlyRecord = new HourlyRecord();

		hourlyRecord.setStationId(stationId);
		hourlyRecord.setSiteId(siteId);
		hourlyRecord.setTimestamp(start.atZone(ZoneId.of("Etc/GMT")).toInstant());
		
		double hourlyPrecipTotal = 0;
		Double minTemp = null;
		Double maxTemp = null;
		Double maxGust = null;
		Double maxWind = null;

		for (Record record : records) {
			
			// Get max data fields
			if (record.getTemp1() != null) {
				if (minTemp != null) {
					if (record.getTemp1() < minTemp)
						minTemp = record.getTemp1();
				} else {
					minTemp = record.getTemp1();
				}

				if (maxTemp != null) {
					if (record.getTemp1() > maxTemp)
						maxTemp = record.getTemp1();
				} else {
					maxTemp = record.getTemp1();
				}
			}
			
			if (record.getWindGust() != null) {

				if (maxGust != null) {
					if (record.getWindGust() > maxGust)
						maxGust = record.getWindGust();
				} else {
					maxGust = record.getWindGust();
				}
			}

			if (record.getWindMax() != null) {

				if (maxWind != null) {
					if (record.getWindMax() > maxWind)
						maxWind = record.getWindMax();
				} else {
					maxWind = record.getWindMax();
				}
			}
			
			// Sum precipitation
			hourlyPrecipTotal += convertNullDoubleToZero(record.getPrecip());
					
			
			if (hourlyRecord.getTemp1() == null) {
				hourlyRecord.setTemp1(record.getTemp1());
			}

			if (hourlyRecord.getTemp2() == null) {
				hourlyRecord.setTemp2(record.getTemp2());
			}

			if (hourlyRecord.getHumidity() == null) {
				hourlyRecord.setHumidity(record.getHumidity());
			}

			if (hourlyRecord.getPressure() == null) {
				hourlyRecord.setPressure(record.getPressure());
			}

			if (hourlyRecord.getWindDir() == null) {
				hourlyRecord.setWindDir(record.getWindDir());
			}

			if (hourlyRecord.getWindSpeed() == null) {
				hourlyRecord.setWindSpeed(record.getWindSpeed());
			}

			if (hourlyRecord.getDewPoint() == null) {
				hourlyRecord.setDewPoint(record.getDewPoint());
			}

			if (hourlyRecord.getHeatIndex() == null) {
				hourlyRecord.setHeatIndex(record.getHeatIndex());
			}

			if (hourlyRecord.getWindChill() == null) {
				hourlyRecord.setWindChill(record.getWindChill());
			}
			
		}

		hourlyRecord.setMinTemp(minTemp);
		hourlyRecord.setMaxTemp(maxTemp);
		hourlyRecord.setWindGust(maxGust);
		hourlyRecord.setWindMax(maxWind);
		hourlyRecord.setPrecip(hourlyPrecipTotal);

		//hourlyRecord.print();

		return hourlyRecord;
	}

	private double convertNullDoubleToZero(Double value) {

		if (value == null)
			return 0;
		return value;
	}

}
