package com.peterchaschke.weather.site;

import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.weather.record.Record;

import lombok.Getter;
import lombok.Setter;

@JsonRootName(value = "site")
@Getter
@Setter
public class SiteModel extends RepresentationModel<SiteModel> {
	
	private Long id;
	private String name;
	private String title;
	private int active;
	private String viewSettings;
	List<Record> records;


}
