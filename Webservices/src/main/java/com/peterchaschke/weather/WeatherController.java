package com.peterchaschke.weather;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peterchaschke.weather.record.DataSource;
import com.peterchaschke.weather.record.IncomingRecord;
import com.peterchaschke.weather.record.PageableWrapper;
import com.peterchaschke.weather.record.Record;
import com.peterchaschke.weather.record.RecordModel;
import com.peterchaschke.weather.record.RecordService;
import com.peterchaschke.weather.security.SecurityConstants;
import com.peterchaschke.weather.site.Site;
import com.peterchaschke.weather.site.SiteService;
import com.peterchaschke.weather.station.Station;
import com.peterchaschke.weather.station.StationDetailsModel;
import com.peterchaschke.weather.station.StationModel;
import com.peterchaschke.weather.station.StationServiceImpl;
import com.peterchaschke.weather.station.StationSummary;
import com.peterchaschke.weather.user.UserService;

@RestController
@RequestMapping("/weather")
public class WeatherController {

	@Autowired
	private StationServiceImpl stationService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private RecordService recordService;

	@Autowired
	private UserService userService;

	Logger logger = LoggerFactory.getLogger(WeatherController.class);

	@GetMapping("/stations")
	public ResponseEntity<List<StationModel>> getAllWeatherStations() {

		List<StationModel> stations = stationService.getAllActiveStations();

		return new ResponseEntity<List<StationModel>>(stations, HttpStatus.OK);
	}
	
	@GetMapping("/stations/summaries")
	public ResponseEntity<List<StationSummary>> getAllWeatherStationsSummaries() {

		List<StationSummary> stations = stationService.getAllStationSummaries();

		return new ResponseEntity<List<StationSummary>>(stations, HttpStatus.OK);
	}
	

	@GetMapping("/stations/{id}/details")
	public ResponseEntity<StationDetailsModel> getWeatherStationDetails(@PathVariable Long id) throws Exception {

		StationDetailsModel stationDetailsModel = stationService.getStationDetailsModel(id);

		return new ResponseEntity<StationDetailsModel>(stationDetailsModel, HttpStatus.OK);
	}

	@GetMapping("/stations/{stationId}/site/{siteId}/data")
	public ResponseEntity<PageableWrapper<Record>> getRecordsBySite(@PathVariable Long stationId,
			@PathVariable Long siteId, @RequestParam("startdate") String startDate,
			@RequestParam("enddate") String endDate, @RequestParam("source") DataSource source,
			@RequestParam("ignorePageSize") Optional<Boolean> ignorePageSize, Pageable pageable) throws Exception {

		stationService.validateStationExistsOrThrowException(stationId);
		siteService.validateSiteExistsOrThrowException(siteId);
	
		PageableWrapper<Record> records = recordService.getRecordsBySiteBetweenDate(stationId, siteId, source,
				startDate, endDate, pageable, ignorePageSize);

		return new ResponseEntity<PageableWrapper<Record>>(records, HttpStatus.OK);

	}
	
	@GetMapping("/stations/{stationId}/site/{siteId}/data/latest")
	public ResponseEntity<RecordModel> getLatestRecord(@PathVariable Long stationId, @PathVariable Long siteId)
			throws Exception {

		RecordModel recordModel = recordService.getLatestRecord(stationId, siteId);
		return new ResponseEntity<RecordModel>(recordModel, HttpStatus.OK);
	}

	@PostMapping("/stations/{stationId}/site/{siteId}/data")
	ResponseEntity<?> newData(HttpServletRequest request, @PathVariable Long stationId, @PathVariable Long siteId,
			@RequestBody IncomingRecord incomingRecord) throws Exception, FailedDependencyException {

		String apiKey = request.getHeader(SecurityConstants.API_KEY_HEADER);
		String apiUser = request.getHeader(SecurityConstants.API_USER_HEADER);

		try {
			userService.authorizeDataRequest(apiKey, apiUser, stationId);

		} catch (Exception e) {

			logger.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Station station = stationService.validateStationExistsOrThrowException(stationId);
		Site site = siteService.validateSiteExistsOrThrowException(siteId);

		try {

			stationService.validateStationWithSite(station.getId(), site.getId());

		} catch (Exception e) {

			logger.error(e.getMessage());

			return new ResponseEntity<Record>(HttpStatus.FAILED_DEPENDENCY);
		}

		recordService.addRecord(station, site, incomingRecord);

		return new ResponseEntity<Record>(HttpStatus.CREATED);

	}

}