package com.peterchaschke.weather.data.truncation.actions;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.peterchaschke.weather.data.truncation.config.ConfigEntry;
import com.peterchaschke.weather.data.truncation.config.ConfigRepository;
import com.peterchaschke.weather.data.truncation.records.RecordRepository;

public class DeleteAction extends TruncationAction {

	private static final Logger logger = LoggerFactory.getLogger(DeleteAction.class);

	private final RecordRepository recordRepository;
	private final ConfigRepository configRepository;

	public DeleteAction(RecordRepository recordRepository, ConfigRepository configRepository) {
		this.recordRepository = recordRepository;
		this.configRepository = configRepository;
	}

	@Override
	public void doTruncation(ConfigEntry config) {

		ZoneId z = ZoneId.of("Etc/GMT");
		LocalDate today = LocalDate.now(z);
		LocalDate dateToRetention = today.minusDays(config.getRetentionDays());

		Instant dateToRetentionInstant = dateToRetention.atStartOfDay(z).toInstant();

		long numRecordsForDeletion = recordRepository.countByStationIdAndSiteIdAndTimestampBefore(config.getStationId(),
				config.getSiteId(), dateToRetentionInstant);

		recordRepository.deleteByStationIdAndSiteIdAndTimestampBefore(config.getStationId(), config.getSiteId(),
				dateToRetentionInstant);
		logger.info(String.format("%o-%o: Deleted %o records before %s from record table.", config.getStationId(),
				config.getSiteId(), numRecordsForDeletion, dateToRetentionInstant.toString()));

		config.setLastConsolidatedDate(dateToRetention);
		configRepository.save(config);
		logger.info(String.format("%o-%o: Set last consolidation date to %s.", config.getStationId(),
				config.getSiteId(), dateToRetention.toString()));

	}

}
