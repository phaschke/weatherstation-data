package com.peterchaschke.weather.security;

public class SecurityConstants {
	
	public static final String API_KEY_HEADER = "API-KEY";
	public static final String API_USER_HEADER = "API-USER";

}
