package com.peterchaschke.weather.truncation;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "data_truncation")
public class TruncationEntry {

	@Id
	private long id;

	@Column(name = "station_id")
	private long stationId;

	@Column(name = "site_id")
	private long siteId;

	private boolean active;

	private String mode;

	@Column(name = "retention_days")
	private int retentionDays;

	@Column(name = "last_consolidated_date", columnDefinition = "TIMESTAMP")
	private Instant lastConsolidatedDate;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getStationId() {
		return this.stationId;
	}

	public void setStationId(long stationId) {
		this.stationId = stationId;
	}

	public long getSiteId() {
		return this.siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}

	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getMode() {
		return this.mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public int getRetentionDays() {
		return this.retentionDays;
	}

	public void setRetentionDays(int retentionDays) {
		this.retentionDays = retentionDays;
	}

	public Instant getLastConsolidatedDate() {
		return this.lastConsolidatedDate;
	}

	public void setLastConsolidatedDate(Instant lastConsolidatedDate) {
		this.lastConsolidatedDate = lastConsolidatedDate;
	}

}


