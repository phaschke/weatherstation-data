package com.peterchaschke.weather.data.truncation.actions;

import com.peterchaschke.weather.data.truncation.config.ConfigEntry;

public abstract class TruncationAction {

	public abstract void doTruncation(ConfigEntry config);
}
