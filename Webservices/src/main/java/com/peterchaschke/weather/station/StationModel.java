package com.peterchaschke.weather.station;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.peterchaschke.weather.site.Site;

@JsonRootName(value = "station")
@Relation(collectionRelation = "stations")
public class StationModel extends RepresentationModel<StationModel> {
	
	private Long id;
	private String name;
	private String displayName;
	private String timezone;
	private String description;
	private int active;
	private String locationDisplay;
	private int elevation;
	private Site site;
	private String viewSettings;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
	public String getLocationDisplay() {
		return locationDisplay;
	}

	public void setLocationDisplay(String locationDisplay) {
		this.locationDisplay = locationDisplay;
	}

	public int getElevation() {
		return elevation;
	}

	public void setElevation(int elevation) {
		this.elevation = elevation;
	}
	
	public Site getSite() {
		return site;
	}
	
	public void setSite(Site site) {
		this.site = site;
	}
	
	public String getViewSettings() {
		return viewSettings;
	}

	public void setViewSettings(String viewSettings) {
		this.viewSettings = viewSettings;
	}

}
