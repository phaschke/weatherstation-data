package com.peterchaschke.weather.data.truncation.actions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.weather.data.truncation.config.ConfigRepository;
import com.peterchaschke.weather.data.truncation.hourlyRecord.HourlyRecordService;
import com.peterchaschke.weather.data.truncation.records.RecordRepository;

@Service
public class TruncationActionFactory {

	@Autowired
	private final RecordRepository recordRepository;
	private final ConfigRepository configRepository;
	private final HourlyRecordService hourlyRecordService;

	TruncationActionFactory(RecordRepository recordRepository, ConfigRepository configRepository,
			HourlyRecordService hourlyRecordService) {
		this.recordRepository = recordRepository;
		this.configRepository = configRepository;
		this.hourlyRecordService = hourlyRecordService;
	}

	public TruncationAction getActionClass(String mode) {

		switch (mode) {

		case "consolidate":
			return new ConsolidateAction(recordRepository, configRepository, hourlyRecordService);

		case "delete":
			return new DeleteAction(recordRepository, configRepository);

		default:
			return null;
		}
	}

}
