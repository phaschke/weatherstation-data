package com.peterchaschke.weather.user;

import org.springframework.stereotype.Service;

import com.peterchaschke.weather.UserAuthorizationException;

@Service
public class UserService {
	
	public final UserRepository userRepository;
	
	UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public void authorizeDataRequest(String apiKey, String apiUser, Long stationId) throws UserAuthorizationException {
		
		
		if (requiredApiKeysExist(apiKey, apiUser)) {
			throw new UserAuthorizationException("API key/user", apiUser, "API key and/or API user not present in request");
		}
		
		// Get API Key from user
		User userEntity = userRepository.findByNameAndActive(apiUser, 1);
		
		if (userEntity == null) {
			throw new UserAuthorizationException("apiUser", apiUser, "API User does not exist");
		}
		
		// Make sure API User has permissions for requested station
		if (userEntity.getStationId().compareTo(stationId) != 0) {
			throw new UserAuthorizationException("apiUser", apiUser, "API User does not have permissions for the requested station");
		}

		// Compare stored key with given key
		if (!userEntity.getApiKey().equals(apiKey)) {
			throw new UserAuthorizationException("apiKey", apiKey, "API Key mismatch");
		}
		
	}
	
	private boolean requiredApiKeysExist(String apiKey, String apiUser) {
		return (apiKey == null || apiUser == null);
	}
	
}
