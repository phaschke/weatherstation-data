package com.peterchaschke.weather.record;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.peterchaschke.weather.FailedDependencyException;
import com.peterchaschke.weather.site.Site;
import com.peterchaschke.weather.site.SiteService;
import com.peterchaschke.weather.station.Station;
import com.peterchaschke.weather.station.StationServiceImpl;
import com.peterchaschke.weather.truncation.HourlyRecordService;

@Service
public class RecordService {

	@Autowired
	private StationServiceImpl stationService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private RecordRepository recordRepository;

	@Autowired
	private HourlyRecordService hourlyRecordService;

	public RecordModel getLatestRecord(Long stationId, Long siteId) throws Exception {

		Station station = stationService.validateStationExistsOrThrowException(stationId);
		siteService.validateSiteExistsOrThrowException(siteId);

		Record record = recordRepository.findFirst1ByStationIdAndSiteIdOrderByTimestampDesc(stationId, siteId);

		// Get todays precipitation sum
		if (record != null) {
			Double precipToday = getPrecipSumTodayByStationSite(station, siteId);
			record.setPrecipToday(precipToday);
			return record.toModel();
		}

		return null;
	}

	private Double getPrecipSumTodayByStationSite(Station station, Long siteId) {

		ZoneId zoneId = ZoneId.of(station.getTimezone());
		ZonedDateTime zonedDateTime = ZonedDateTime.now(zoneId);
		Double precipToday = recordRepository.getPrecipToday(station.getId(), siteId, zonedDateTime.toLocalDate());

		return precipToday;
	}

	public PageableWrapper<Record> getRecordsBySiteBetweenDate(Long stationId, Long siteId, DataSource source,
			String startDate, String endDate, Pageable pageable, Optional<Boolean> ignorePageSize) throws Exception {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
		LocalDateTime start = LocalDateTime.parse(startDate, formatter);
		LocalDateTime end = LocalDateTime.parse(endDate, formatter);

		// TODO Check if start date is before end date?

		ZoneId zoneId = ZoneId.of("Etc/GMT");
		Instant startInstant = start.atZone(zoneId).toInstant();
		Instant endInstant = end.atZone(zoneId).toInstant();

		PageableWrapper<Record> pageableWrapper = new PageableWrapper<Record>();

		if (source.equals(DataSource.RECENT)) {
			
			
			int totalRecords = recordRepository.countAllByStationIdAndSiteIdAndTimestampBetween(stationId, siteId,
					startInstant, endInstant);
			
			if(ignorePageSize.isPresent() && (ignorePageSize.get() == true)) {
				pageable = PageRequest.of(0, totalRecords+1, pageable.getSort());
			}

			// All records come from the data table
			List<Record> records = getRecordsFromDataTable(stationId, siteId, startInstant, endInstant, pageable);
			

			return buildPageableWrapper(records, pageable, totalRecords);

		} else if (source.equals(DataSource.TRUNCATED)) {
			
			int totalRecords = hourlyRecordService.getTotalHourlyRecordCount(stationId, siteId, startInstant,
					endInstant);

			if(ignorePageSize.isPresent() && (ignorePageSize.get() == true)) {
				pageable = PageRequest.of(0, totalRecords+1, pageable.getSort());
			}

			// All records come from the hourly table
			List<Record> records = getRecordsFromHourlyTable(stationId, siteId, startInstant, endInstant, pageable);
			
			return buildPageableWrapper(records, pageable, totalRecords);

		} else if (source.equals(DataSource.BOTH)) {
			
			int totalRecent = recordRepository.countAllByStationIdAndSiteIdAndTimestampBetween(stationId, siteId, startInstant,
					endInstant);
			int totalHourly = hourlyRecordService.getTotalHourlyRecordCount(stationId, siteId, startInstant, endInstant);
			int totalRecords = totalRecent + totalHourly;
			
			if(ignorePageSize.isPresent() && (ignorePageSize.get() == true)) {
				pageable = PageRequest.of(0, totalRecords+1, pageable.getSort());
			}

			return getRecordsFromBothTables(stationId, siteId, startInstant, endInstant, pageable, totalRecent, totalHourly, totalRecords);

		} else {

			return pageableWrapper;
		}

	}

	private PageableWrapper<Record> getRecordsFromBothTables(Long stationId, Long siteId, Instant start, Instant end,
			Pageable pageable, int totalRecent, int totalHourly, int totalRecords) throws Exception {

		/*int totalRecent = recordRepository.countAllByStationIdAndSiteIdAndTimestampBetween(stationId, siteId, start,
				end);
		int totalHourly = hourlyRecordService.getTotalHourlyRecordCount(stationId, siteId, start, end);
		int totalRecords = totalRecent + totalHourly;*/

		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();

		//System.out.println("pageNumber: " + pageNumber + " pageSize: " + pageSize + " totalRecent: " + totalRecent
		//		+ " totalRecords: " + totalRecords + " totalHourly: " + totalHourly);

		if (totalRecords == 0) {
			// No requested records found in either table
			List<Record> records = new ArrayList<Record>();
			return buildPageableWrapper(records, pageable, totalRecords);

		} else if ((pageNumber + 1) * pageSize <= totalRecent) {
			// Requested records fall between the recent record count
			// Get all records from the recent table
			//System.out.println("RECENT");

			List<Record> records = getRecordsFromDataTable(stationId, siteId, start, end, pageable);

			return buildPageableWrapper(records, pageable, totalRecords);

		} else if ((pageNumber * pageSize) + 1 > totalRecent) {

			// All requested records fall in the hourly record table
			// However, an offset from the boundary case must be considered
			//System.out.println("HOURLY");

			List<Record> records = new ArrayList<Record>();

			if (totalHourly == totalRecords) {
				// Total records are hourly

				records = getRecordsFromHourlyTable(stationId, siteId, start, end, pageable);

			} else {
				// All record in current page are hourly, but not totally hourly
				// Get last hourly record from previous page to use as new start time
				int totalHourlySinceRecent = ((pageNumber) * pageSize) - totalRecent;
				int hourlyOffset = totalHourlySinceRecent % pageSize;
				int pageOffset = totalHourlySinceRecent / pageSize;
				// System.out.println("totalHourlySinceRecent: "+totalHourlySinceRecent);
				// System.out.println("hourlyOffset: "+hourlyOffset);
				// System.out.println("pageOffset: "+pageOffset);

				Pageable hourlyPageable = PageRequest.of(0, hourlyOffset, Sort.by(Direction.DESC, "timestamp"));
				List<Record> hourlyRecords = hourlyRecordService.getRecordsFromHourlyData(stationId, siteId, start, end,
						hourlyPageable);

				Record lastHourlyRecord = hourlyRecords.get(hourlyRecords.size() - 1);
				Instant newEnd = lastHourlyRecord.getTimestamp();
				newEnd = newEnd.minusSeconds(1);

				Pageable newHourlyPageable = PageRequest.of(pageOffset, pageable.getPageSize(),
						Sort.by(Direction.DESC, "timestamp"));

				records = getRecordsFromHourlyTable(stationId, siteId, start, newEnd, newHourlyPageable);
			}

			return buildPageableWrapper(records, pageable, totalRecords);

			// } else(((pageNumber*pageSize)+1 > totalRecent) && ((pageNumber+1)*pageSize >
			// totalRecent)) {
		} else {
			// Requested records max falls within hourly, and min in recent
			// Get records from both recent and hourly tables, combine and build pageable
			// response
			//System.out.println("RECENT + HOURLY");

			List<Record> recentRecords = getRecordsFromDataTable(stationId, siteId, start, end, pageable);
			int fromRecent = recentRecords.size();

			int remainingFromHourly = (pageSize - fromRecent);

			Pageable hourlyPageable = PageRequest.of(0, remainingFromHourly, Sort.by(Direction.DESC, "timestamp"));
			List<Record> hourlyRecords = hourlyRecordService.getRecordsFromHourlyData(stationId, siteId, start, end,
					hourlyPageable);

			List<Record> records = Stream.concat(recentRecords.stream(), hourlyRecords.stream())
					.collect(Collectors.toList());

			return buildPageableWrapper(records, pageable, totalRecords);

		}
	}
	
	private PageableWrapper<Record> buildPageableWrapper(List<Record> records, Pageable pageable, int totalRecords) {

		PageableWrapper<Record> pageableWrapper = new PageableWrapper<Record>();

		//System.out.println(pageable.getPageNumber());
		//System.out.println("Page size: " + pageable.getPageSize());
		//System.out.println(totalRecords);

		pageableWrapper.setContent(records);
		pageableWrapper.setPage(pageable.getPageNumber());
		pageableWrapper.setTotalElements(totalRecords);
		pageableWrapper.setPageSize(pageable.getPageSize());
		if (pageable.getPageNumber() == 0) {
			pageableWrapper.setFirst(true);
		}
		if ((pageable.getPageNumber() + 1) * pageable.getPageSize() >= totalRecords) {
			pageableWrapper.setLast(true);
		}
		pageableWrapper.setTotalPages((totalRecords / pageable.getPageSize()));
		
		String sortStr = pageable.getSort().toString();
		String[] sortArr = sortStr.split(":");
		if(sortArr.length >= 2) {
			pageableWrapper.setSortField(sortArr[0].trim());
			pageableWrapper.setSortDirection(Direction.fromString(sortArr[1].trim()));
		}

		return pageableWrapper;
	}

	private List<Record> getRecordsFromDataTable(Long stationId, Long siteId, Instant start, Instant end,
			Pageable pageable) {

		List<Record> records = null;

		records = recordRepository.findAllByStationIdAndSiteIdAndTimestampBetween(stationId, siteId, start, end,
				pageable);

		return records;
	}

	private List<Record> getRecordsFromHourlyTable(Long stationId, Long siteId, Instant start, Instant end,
			Pageable pageable) {

		List<Record> records = hourlyRecordService.getRecordsFromHourlyData(stationId, siteId, start, end, pageable);

		return records;
	}

	public Record addRecord(Station station, Site site, IncomingRecord incomingRecord) throws Exception, FailedDependencyException {
		
		Record record = incomingRecord.toRecord();

		record.setStationId(station.getId());
		record.setSiteId(site.getId());

		if (record.getTimestamp() == null) {

			Instant generatedTimestamp = generateTimestamp(station);
			record.setTimestamp(generatedTimestamp);
		}

		// if(site.getDaylightSavingsTimeActive()) {
		// record.setTimestamp(handleDaylightSavingsTime(record.getTimestamp()));
		// }

		if (site.isCalcDewPoint()) {
			record.setDewPoint(calculateDewPoint(record.getTemp1(), record.getHumidity()));
		}
		if (site.isCalcHeatIndex()) {
			record.setHeatIndex(calculateHeatIndex(record.getTemp1(), record.getHumidity()));
		}
		if (site.isCalcWindChill()) {
			record.setWindChill(calculateWindChill(record.getTemp1(), record.getWindSpeed()));
		}

		// printRecord(record);

		Record savedRecord = recordRepository.save(record);

		return savedRecord;
	}

	private Instant generateTimestamp(Station station) throws Exception {

		if (station.getTimezone() == null) {

			throw new FailedDependencyException("timezone", null, "Unable to generate timezone, timezone not present in station record");
		}

		ZoneId zoneId = ZoneId.of(station.getTimezone());
		ZonedDateTime zonedDateTime = ZonedDateTime.now(zoneId);
		LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();
		Instant timestampInstant = localDateTime.atZone(ZoneId.of("Etc/GMT")).toInstant();
		
		return timestampInstant;
		
	}

	/*
	 * private String handleDaylightSavingsTime(String inputTimeStamp) {
	 * 
	 * try { String convertedTimestamp = inputTimeStamp.trim().replace(" ", "T");
	 * 
	 * LocalDateTime timestamp = LocalDateTime.parse(convertedTimestamp); timestamp
	 * = timestamp.minusHours(1);
	 * 
	 * return timestamp.toString().replace("T", " ");
	 * 
	 * } catch(Exception e) {
	 * 
	 * System.out.println("Failed to convert time: "+inputTimeStamp);
	 * 
	 * return inputTimeStamp; } }
	 */

	private Double calculateDewPoint(Double t, Double rh) {

		try {

			// http://bmcnoldy.rsmas.miami.edu/Humidity.html
			// 243.04*(LN(RH/100)+((17.625*T)/(243.04+T)))/(17.625-LN(RH/100)-((17.625*T)/(243.04+T)))
			t = (t - 32) * 5 / 9;
			Double dewPoint = 243.04 * (Math.log(rh / 100) + ((17.625 * t) / (243.04 + t)))
					/ (17.625 - Math.log(rh / 100) - ((17.625 * t) / (243.04 + t)));
			dewPoint = (dewPoint * 9 / 5) + 32;

			BigDecimal bd = new BigDecimal(Double.toString(dewPoint));

			bd = bd.setScale(1, RoundingMode.HALF_UP);
			return bd.doubleValue();

		} catch (Exception e) {
			return null;
		}

	}

	private Double calculateHeatIndex(Double t, Double rh) {

		// If temperature is below 80 degree F, heat index calculation is invalid (as
		// per NWS)
		if (t < 80) {
			return null;
		}

		try {
			// https://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml
			// 0.5 * {T + 61.0 + [(T-68.0)*1.2] + (RH*0.094)}
			Double simpleHI = 0.5 * (t + 61.0 + ((t - 68.0) * 1.2) + (rh * 0.094));
			// In practice, the simple formula is computed first and the result averaged
			// with the temperature.
			Double avgSimpleHI = (simpleHI + t) / 2;

			// System.out.println("Simple Heat Index: "+avgSimpleHI);

			// If this heat index value is 80 degrees F or higher, the full regression
			// equation is applied.
			if (avgSimpleHI >= 80) {

				Double heatIndex = -42.379 + (2.04901523 * t) + 10.14333127 * rh - (.22475541 * t * rh)
						- (.00683783 * t * t) - (.05481717 * rh * rh) + (.00122874 * t * t * rh)
						+ (.00085282 * t * rh * rh) - (.00000199 * t * t * rh * rh);
				// System.out.println("Before adj: "+heatIndex);

				// If the RH is less than 13% and the temperature is between 80 and 112 degrees
				// F, then the following adjustment is subtracted from the HI
				if (rh < 13 && (t > 80 && t < 112)) {
					// ADJUSTMENT = [(13-RH)/4]*SQRT{[17-ABS(T-95.)]/17}
					Double adjustment = ((13 - rh) / 4) * Math.sqrt((17 - Math.abs(t - 95)) / 17);
					// System.out.println("Adjustment 1: "+adjustment);
					heatIndex = heatIndex - adjustment;
				}
				// If the RH is greater than 85% and the temperature is between 80 and 87
				// degrees F, then the following adjustment is added to HI
				if (rh > 85 && (t > 80 && t < 87)) {
					// ADJUSTMENT = [(RH-85)/10] * [(87-T)/5]
					Double adjustment = ((rh - 85) / 10) * ((87 - t) / 5);
					// System.out.println("Adjustment 2: "+adjustment);
					heatIndex = heatIndex + adjustment;
				}

				BigDecimal bd = new BigDecimal(Double.toString(heatIndex));
				bd = bd.setScale(1, RoundingMode.HALF_UP);
				// System.out.println("HI: "+bd.doubleValue());
				return bd.doubleValue();

			} else {
				// Round result
				BigDecimal bd = new BigDecimal(Double.toString(avgSimpleHI));
				bd = bd.setScale(1, RoundingMode.HALF_UP);
				return bd.doubleValue();
			}

		} catch (Exception e) {
			return null;
		}
	}

	// https://www.weather.gov/media/epz/wxcalc/windChill.pdf
	private Double calculateWindChill(Double t, Double windSpeed) {

		// The wind chill calculator only works for temperatures at or below 50 ° F and
		// wind speeds above 3 mph.
		if (windSpeed == null) {
			return null;
		}
		if (t > 50 || windSpeed < 3) {
			return null;
		}
		try {
			Double windChill = 35.74 + (0.6215 * t) - (35.75 * (Math.pow(windSpeed, 0.16)))
					+ ((0.4275 * t) * Math.pow(windSpeed, 0.16));

			BigDecimal bd = new BigDecimal(Double.toString(windChill));
			bd = bd.setScale(1, RoundingMode.HALF_UP);
			return bd.doubleValue();

		} catch (Exception e) {
			return null;
		}
	}

}
