package com.peterchaschke.weather.station;

import java.util.List;
import java.util.Optional;

import com.peterchaschke.weather.EntityNotFoundException;

public interface StationService {
	
	public List<Station> getAllStations();
	public List<StationSummary> getAllStationSummaries();
	public List<StationModel> getAllActiveStations();
	public Optional<Station> findStationById(Long stationId);
	public StationDetailsModel getStationDetailsModel(Long stationId) throws Exception;
	public Station validateStationExistsOrThrowException(Long stationId) throws EntityNotFoundException;
	public void validateStationWithSite(Long stationId, Long siteId) throws Exception;
}
