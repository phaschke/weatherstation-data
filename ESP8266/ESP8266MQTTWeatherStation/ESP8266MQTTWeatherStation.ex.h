#ifndef _ESP8266MQTTWEATHERSTATION.H
#define _ESP8266MQTTWEATHERSTATION.H

// WiFi Connection
#define SSIDD ""
#define PASSWORD ""

// Station details
#define HOST ""
#define HTTPS_PORT = #
#define REQUEST_URL ""
#define STATION #
#define SITE #
#define API_USER ""
#define API_KEY ""
#define SEND_DURATION # // In seconds

// Device Settings
#define DEBUG 1
#define LED_PIN LED_BUILTIN

// One Wire Settings
#define ONEWIREBUS #

// MQTT Settings
#define DEVICE_NAME ""
#define MQTT_SERVER ""
#define MQTT_PORT 1883
#define IN_TOPIC ""
#define OUT_TOPIC ""

#endif //_ESP8266MQTTWEATHERSTATION.H
