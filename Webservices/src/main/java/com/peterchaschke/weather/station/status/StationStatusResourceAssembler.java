package com.peterchaschke.weather.station.status;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class StationStatusResourceAssembler extends RepresentationModelAssemblerSupport<StationStatusResource, StationStatusResource> {
	
	public StationStatusResourceAssembler() {
		super(StationStatusController.class, StationStatusResource.class);
	}
	
	@Override
	public StationStatusResource toModel(StationStatusResource model) {

		try {
			model.add(linkTo(methodOn(StationStatusController.class).getStatus(model.getId())).withSelfRel());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}

}
