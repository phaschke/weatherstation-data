package com.peterchaschke.weather.station;

public class StationStatusException extends RuntimeException {

	private static final long serialVersionUID = 6878693052233236509L;

	StationStatusException(Long id, String message) {
		super("Failed to get station {"+id+ "} status, Reason: "+message);
	}

}