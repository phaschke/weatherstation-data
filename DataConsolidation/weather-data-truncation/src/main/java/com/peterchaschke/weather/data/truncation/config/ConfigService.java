package com.peterchaschke.weather.data.truncation.config;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.weather.data.truncation.actions.TruncationAction;
import com.peterchaschke.weather.data.truncation.actions.TruncationActionFactory;

@Service
public class ConfigService {

	private static final Logger logger = LoggerFactory.getLogger(ConfigService.class);

	@Autowired
	private final ConfigRepository configRepository;
	private final TruncationActionFactory truncationActionFactory;

	ConfigService(ConfigRepository configRepository, TruncationActionFactory truncationActionFactory) {
		this.configRepository = configRepository;
		this.truncationActionFactory = truncationActionFactory;
	}

	public void getConfigAndDoTruncation() {

		List<ConfigEntry> configEntries = getConfigEntries();

		if (!validateConfigEntriesExist(configEntries)) {
			return;
		}

		doTruncation(configEntries);
	}

	private List<ConfigEntry> getConfigEntries() {

		List<ConfigEntry> configEntries = configRepository.findByActive(true);

		return configEntries;
	}

	private boolean validateConfigEntriesExist(List<ConfigEntry> configEntries) {

		if (configEntries.size() > 0) {
			return true;
		}
		return false;
	}

	private void doTruncation(List<ConfigEntry> configEntries) {

		for (ConfigEntry config : configEntries) {

			if (!validateRetentionDate(config)) {
				logger.info(String.format("%o-%o: Truncation up to retention date for station, skipping.",
						config.getStationId(), config.getSiteId()));
				continue;
			}

			TruncationAction actionClass = truncationActionFactory.getActionClass(config.getMode());

			actionClass.doTruncation(config);
		}
	}

	private boolean validateRetentionDate(ConfigEntry config) {

		LocalDate today = LocalDate.now();
		today = today.minusDays(config.getRetentionDays());

		// Check to see if last truncated date is before the current date less the
		// retention date
		LocalDate lastRetentionDate = config.getLastConsolidatedDate();

		return lastRetentionDate.isBefore(today);

	}

}
