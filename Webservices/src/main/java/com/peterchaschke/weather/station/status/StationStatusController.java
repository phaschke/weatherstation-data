package com.peterchaschke.weather.station.status;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/weather/stations/status")
public class StationStatusController {
	
	@Autowired
	private StationStatusServiceImpl stationStatusService;
	
	@Autowired
	private StationStatusResourceAssembler stationStatusResourceAssembler;
	
	@GetMapping("/{stationId}")
	public ResponseEntity<StationStatusResource> getStatus(@PathVariable Long stationId) {
		
		StationStatusResource stationStatusResource = stationStatusService.getStationStatus(stationId);
		
		return new ResponseEntity<StationStatusResource>(stationStatusResourceAssembler.toModel(stationStatusResource), HttpStatus.OK);
	}
	
	@PutMapping("/{stationId}")
	public ResponseEntity<StationStatusResource> updateStatus(HttpServletRequest request, @PathVariable Long stationId, @RequestBody StationStatusResource stationStatusResource) {
		
		StationStatusResource savedStatusResource = stationStatusService.updateStationStatus(request, stationId, stationStatusResource);
		
		return new ResponseEntity<StationStatusResource>(stationStatusResourceAssembler.toModel(savedStatusResource), HttpStatus.OK);
	}
}
