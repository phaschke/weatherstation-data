package com.peterchaschke.weather.data.truncate;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
 entityManagerFactoryRef = "weatherEntityManagerFactory",
 transactionManagerRef = "weatherTransactionManager",
 basePackages = {"com.peterchaschke.weather.data.truncate"}
 )
public class WeatherDataSourceConfiguration {
	
	@Autowired 
	Environment env;
	
	@Primary
	@Bean(name="weatherDataSource")
	//@ConfigurationProperties(prefix="app.datasource.weather")
	public DataSource weatherDataSource(){
		System.out.println("weatherDataSource");
		//return DataSourceBuilder.create().build();
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
	    dataSource.setUrl(env.getProperty("app.datasource.weather.jdbc-url"));
	    dataSource.setUsername(env.getProperty("app.datasource.weather.username"));
	    dataSource.setPassword(env.getProperty("app.datasource.weather.password"));

	    return dataSource;
	}
	
	@Primary
	@Bean(name = "weatherEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean weatherEntityManagerFactory( EntityManagerFactoryBuilder builder, 
			@Qualifier("weatherDataSource") DataSource weatherDataSource ) {
	  return builder
	   .dataSource(weatherDataSource)
	   .packages("com.peterchaschke.weather.data.truncate")
	   .persistenceUnit("db1")
	   .build();
	 }
	
	@Primary
	@Bean(name = "weatherTransactionManager")
	public PlatformTransactionManager weatherTransactionManager(
			@Qualifier("weatherEntityManagerFactory") EntityManagerFactory weatherEntityManagerFactory) {
	  return new JpaTransactionManager(weatherEntityManagerFactory);
	 }

}
