package com.peterchaschke.weather.station.status;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.weather.FailedDependencyException;
import com.peterchaschke.weather.ResourceNotFoundException;
import com.peterchaschke.weather.UserAuthorizationException;
import com.peterchaschke.weather.security.SecurityConstants;
import com.peterchaschke.weather.station.StationRepository;
import com.peterchaschke.weather.user.UserService;

@Service
public class StationStatusServiceImpl implements StationStatusService {
	
	@Autowired
	private StationStatusRepository stationStatusRepository;
	
	@Autowired
	private StationRepository stationRepository;
	
	@Autowired
	private UserService userService;
	
	public StationStatusResource getStationStatus(Long stationId) {
		
			stationRepository.findById(stationId)
				.orElseThrow(() -> new ResourceNotFoundException("stationId", stationId, "No stations found."));
				
			Optional<StationStatus> optStatus = stationStatusRepository.findOneByStationId(stationId);
			
			if(optStatus.isEmpty()) {
				throw new ResourceNotFoundException("status", stationId, "No status found.");
			}
			
			StationStatus stationStatus = optStatus.get();
			
			return stationStatus.toResource();
	}
	
	public StationStatusResource updateStationStatus(HttpServletRequest request, Long stationId, StationStatusResource stationStatusResource) throws UserAuthorizationException {
		
		stationRepository.findById(stationId)
			.orElseThrow(() -> new ResourceNotFoundException("stationId", stationId, "No stations found."));
		
		
		String apiKey = request.getHeader(SecurityConstants.API_KEY_HEADER);
		String apiUser = request.getHeader(SecurityConstants.API_USER_HEADER);

		userService.authorizeDataRequest(apiKey, apiUser, stationId);
		
		if(stationStatusResource == null) {
			throw new FailedDependencyException("stationStatusResource", stationStatusResource, "Station status request must not be null");
		}
		
		Optional<StationStatus> optStatus = stationStatusRepository.findOneByStationId(stationId);
		
		StationStatus entity = new StationStatus();
		
		if(optStatus.isPresent()) {
			
			// Update existing status entry
			entity.setId(optStatus.get().getId());
		}
		
		StationStatus incomingStatus = stationStatusResource.toEntity();
		
		entity.setStationId(stationId);
		entity.setTimestamp(incomingStatus.getTimestamp());
		entity.setSdStatus(incomingStatus.getSdStatus());
		entity.setRtcStatus(incomingStatus.getRtcStatus());
		entity.setBmeStatus(incomingStatus.getBmeStatus());
		entity.setTemp1Status(incomingStatus.getTemp1Status());
		entity.setTemp2Status(incomingStatus.getTemp2Status());
		entity.setLightStatus(incomingStatus.getLightStatus());
		entity.setRtcBattVStatus(incomingStatus.getRtcBattVStatus());
		entity.setWeatherPost(incomingStatus.getWeatherPost());
		entity.setRebootTime(incomingStatus.getRebootTime());
		entity.setAuxPower(incomingStatus.getAuxPower());
		entity.setAuxStatus1(incomingStatus.getAuxStatus1());
		entity.setAuxStatus2(incomingStatus.getAuxStatus3());
		entity.setAuxStatus2(incomingStatus.getAuxStatus3());
		
		StationStatus savedStatus = stationStatusRepository.save(entity);
		
		return savedStatus.toResource();
	}
}
