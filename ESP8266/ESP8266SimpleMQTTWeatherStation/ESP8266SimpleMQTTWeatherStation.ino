/*
   ESP8266 Weather Station
*/
#include "ESP8266SimpleMQTTWeatherStation.h"

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

//-- Dallas One wire --
#include <OneWire.h>
#include <DallasTemperature.h>
//--
//-- BME280 --
#include <Wire.h>
#include <Adafruit_BME280.h>
//--

// Timer global variables
unsigned long delayStart = 0; // the time the delay started
unsigned long totalDelay;
bool delayRunning = false; // true if still waiting for delay to finish

// Global status variables
boolean useBME = true;

// Variables used for queue
struct Record {
  char timestamp[20];
  char temp1[5];
  char temp2[5];
  char humidity[5];
  char pressure[7];
  char windDir[2];
  float windSpeed;
};
Record recordArray[QUEUE_MAX];
int front = 0;
int rear = -1;
int itemCount = 0;
int requestRetries = 0;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONEWIREBUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

//#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme; // I2C

WiFiClientSecure client;
WiFiClient wifiClient;
PubSubClient pubSubClient(wifiClient);

// ===============================================================
// Setup
// ===============================================================
void setup() {

  Serial.begin(115200);

  delay(3000);

  // Connect to WiFi
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSIDD, PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {

    digitalWrite(LED_PIN, HIGH); // LED On
    delay(2000);
    digitalWrite(LED_PIN, LOW); // LED Off

#if DEBUG
    Serial.println("Connecting to WiFi..");
#endif
  }

#if DEBUG
  Serial.println("Connected to the WiFi network");
#endif

  // Configure MQTT server and callback
  pubSubClient.setServer(MQTT_SERVER, MQTT_PORT);
  pubSubClient.setCallback(callback);

  digitalWrite(LED_PIN, HIGH); // LED Off

  // Configure secure Wifi in insecure mode
  // (Still encrypts messages, but does not verify reciever)
  client.setInsecure();

  // One wire Initialization (For DS18B20)--
  sensors.begin();

  // BME280 Initialization
  bool statusBME;
  statusBME = bme.begin(0x76);
  if (!statusBME) {
#if DEBUG
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
#endif
    // Do not block on BME connection
    useBME = false;
  }

  // Start timer for sending weather data
  // Timer duration set in header file
  delayRunning = true;
  delayStart = millis();
  totalDelay = SEND_DURATION * 1000;

  //collectData();
  //sendData();
}

// ===============================================================
// Loop
// ===============================================================
void loop() {

  // Connect to MQTT
  // Allow weather data to upload reguardless of MQTT connection
  if (!pubSubClient.connected()) {
    reconnect();
  }

  // Check timer for sending weather data
  // Check if there is a active running timer
  if (delayRunning && ((millis() - delayStart) >= totalDelay)) {

#if DEBUG
    Serial.println("End of delay, sending weather data");
#endif

    collectData();
    sendData();

    delayRunning = true;
    delayStart = millis();
    totalDelay = SEND_DURATION * 1000;
  }

  delay(100);

  pubSubClient.loop();

}

// ===============================================================
// Handle connection to MQTT
// ===============================================================
void reconnect() {

#if DEBUG
  Serial.println("Attempting MQTT connection...");
#endif

  // If you do not want to use a username and password, change next line to
  if (pubSubClient.connect(DEVICE_NAME)) {
    //if (client.connect(DEVICE_NAME, mqtt_user, mqtt_password)) {

#if DEBUG
    Serial.println("Connected to MQTT");
#endif
    // Once connected, publish an announcement...
    //client.publish(IN_TOPIC, "Sonoff: booted");
    // ... and resubscribe
    pubSubClient.publish(OUT_TOPIC, "Client Connected");
    pubSubClient.subscribe(IN_TOPIC);

  } else {

#if DEBUG
    Serial.print("failed, rc=");
    Serial.print(pubSubClient.state());
    Serial.println(" try again in 5 seconds");
#endif
    // Wait 5 seconds before retrying
    digitalWrite(LED_PIN, HIGH); // LED On
    delay(5000);
    digitalWrite(LED_PIN, LOW); // LED Off

  }
}

// ===============================================================
// Collect weather conditions
// ===============================================================
void collectData() {

  struct Record record;

  // Collect sensor data and build request
  float temperatureF, humidityF, pressureF;
  char temperature[5];
  char humidity[5];
  char pressure[7];

  sensors.requestTemperatures();

  temperatureF = sensors.getTempFByIndex(0);
  sprintf(temperature, "%0.1f\0", temperatureF);
  //Serial.println(temperatureF);
  //Serial.println(temperature);
  strcpy(record.temp1, temperature);

  if (useBME) {
    humidityF = bme.readHumidity();
    sprintf(humidity, "%0.1f\0", humidityF);
    //Serial.println(humidityF);
    //Serial.println(humidity);
    strcpy(record.humidity, humidity);

    pressureF = (bme.readPressure() / 100.0F);
    sprintf(pressure, "%0.1f\0", pressureF);
    //Serial.println(pressureF);
    //Serial.println(pressure);
    strcpy(record.pressure, pressure);
  }

  //char timestamp[20] = {'2','0','2','0','-','0','7','-','1','1','T','1','3',':','0','1',':','0','0','\0'};
  //strcpy(record.timestamp, timestamp);
  //strcpy(record.temp2, ##);
  //strcpy(record.windDir, "--");
  //record.windSpeed = 6.4;

  // If the queue is full, remove the earliest record
  if (isFull()) {
    requestRetries = 0; // Reset post retry value
    removeData();
#if DEBUG
    Serial.println("Queue is full, removed earliest record");
#endif
  }

  insert(record);
#if DEBUG
  Serial.println("Inserted record in queue");
#endif

  //int queueSize = size();
  //Serial.print("Size: ");
  //Serial.println(queueSize);

  //Serial.println("Peek Record");
  //Record aRecord = peek();

  //Serial.println(aRecord.temp1);
  //Serial.println(aRecord.humidity);
  //Serial.println(aRecord.pressure);

}

// ===============================================================
// Send weather conditions
// ===============================================================
void sendData() {

  // Clear out queue
  while (size() > 0) {

    // Attempt connection to webservice and POST request
    if (client.connect(HOST, HTTPS_PORT)) {

#if DEBUG
      Serial.println("Successfully connected to webservice, retrieving record");
#endif

      //Serial.println("Peek Record");
      Record peekRecord = peek();

      StaticJsonDocument<256> requestJson;

      //requestJson["timestamp"] = "2020-06-03T23:01:05";
      requestJson["temp1"] = peekRecord.temp1;
      requestJson["humidity"] = peekRecord.humidity;
      requestJson["pressure"] = peekRecord.pressure;

      // Convert JSON request into string
      char buffer[256];
      size_t n = serializeJson(requestJson, buffer);
      String request = String(buffer);

      client.println(String("POST ") + REQUEST_URL + " HTTP/1.1");
      client.println("Host: " + (String)HOST);
      client.println("User-Agent: ESP8266/1.0");
      client.println("Connection: close");
      client.println("Content-Type: application/json");
      client.print("Content-Length: ");
      client.println(request.length());
      client.print("API-USER: ");
      client.println(API_USER);
      client.print("API-KEY: ");
      client.println(API_KEY);
      client.println();
      client.println(request);
      //delay(10);

#if DEBUG
      Serial.println("Request sent");
#endif

      //while (client.available() == 0);
      unsigned long timeout = millis();
      while (client.available() == 0) {
        if (millis() - timeout > 5000) {
#if DEBUG
          Serial.println("Client timed out while awaiting a response.");
#endif
          client.stop();
          return;
        }
      }

      while (client.available()) {
        String line = client.readStringUntil('\n');
        //Serial.println(line);
        if (line.startsWith("HTTP/1.1") && line.length() > 12) {

          //Serial.println(line.substring(9, 12));
          if (line.substring(9, 12) == "201") {

#if DEBUG
            Serial.println("Successfully posted request");
#endif
            removeData(); // Remove successfully posted record from queue

            client.stop(); // Stop the client connection

            delay(1000); // Delay between data requests

          } else {
#if DEBUG
            Serial.print("Failed to post: ");
            Serial.print("HTTP ");
            Serial.println(line.substring(9, 12));
#endif

            // Try to send a failed record up to 3 times before removing it
            if (requestRetries < POST_RETRIES-1) {
              requestRetries++;
#if DEBUG
              Serial.print("Failed to post request, will retry next time around, try: ");
              Serial.println(requestRetries);
#endif
            } else {
#if DEBUG     
              Serial.print("Post attempt ");
              Serial.print(POST_RETRIES);
              Serial.println(", removing record from queue...");
#endif
              removeData();
              requestRetries = 0;
            }

            client.stop(); // Stop the client connection

            // Error posting record, stop and try again
            return;
          }

          break;

        }
      } // End while loop for reading response
    } // End client connect
  } // End while loop for clearing out record queue
}

// ===============================================================
// Code to be run when a MQTT message is recieved
// ===============================================================
void callback(char* topic, byte* payload, unsigned int length) {

  //client.publish(IN_TOPIC, "In callback");

  StaticJsonDocument<255> doc;

  char messageBuf[256];
  int toggleState;
  int duration;
  int i = 0;
  const size_t CAPACITY = JSON_ARRAY_SIZE(10);
  const size_t CAPACITY_OBJ = JSON_OBJECT_SIZE(2);

  char temperature[3];
  char humidity[3];

  // Limit length of input message
  if (length > 255) {
    length = 255;
  }

  // Convert byte array to char array for processing
  for (i = 0; i < length; i++) {
    messageBuf[i] = (char)payload[i];
  }
  messageBuf[i] = '\0';

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, messageBuf);

  // Test if parsing succeeds.
  if (error) {
    //TODO: Send back error
#if DEBUG
    Serial.print(("deserializeJson() failed: "));
    Serial.println(error.c_str());
#endif

    return;
  }

  const char* msgId = doc["msgId"];
  if (msgId) {

    // Allocate the memory for the output document
    StaticJsonDocument<512> outputDoc;

#if DEBUG
    Serial.print("Recieved MQTT message with id: ");
    Serial.println(msgId);
#endif
    outputDoc["resId"] = msgId;

#if DEBUG
    Serial.println("Get status.");
#endif

    sensors.requestTemperatures();

    float temperatureF = sensors.getTempFByIndex(0);
    sprintf(temperature, "%0.1f\0", temperatureF);

#if DEBUG
    Serial.print("Current Temp: ");
    Serial.print(temperature);
    Serial.println("ºF");
#endif

    outputDoc["temp"] = temperature;

    if (useBME) {
      float humidityF = bme.readHumidity();
      sprintf(humidity, "%0.1f\0", humidityF);
      outputDoc["humid"] = humidity;

    } else {
      outputDoc["humid"] = -1;
    }

    char buffer[512];
    size_t n = serializeJson(outputDoc, buffer);
    pubSubClient.publish(OUT_TOPIC, buffer);

#if DEBUG
    Serial.println(buffer);
#endif

    return;
  }
  // No message id, do nothing
  return;
}

// ===============================================================
// Record queue helper functions
// ===============================================================
Record peek() {
  return recordArray[front];
}

bool isEmpty() {
  return itemCount == 0;
}

bool isFull() {
  return itemCount == QUEUE_MAX;
}

int size() {
  return itemCount;
}

void insert(Record data) {

  if (!isFull()) {

    if (rear == QUEUE_MAX - 1) {
      rear = -1;
    }

    recordArray[++rear] = data;
    itemCount++;
  }
}

Record removeData() {
  Record data = recordArray[front++];

  if (front == QUEUE_MAX) {
    front = 0;
  }

  itemCount--;
  return data;
}
