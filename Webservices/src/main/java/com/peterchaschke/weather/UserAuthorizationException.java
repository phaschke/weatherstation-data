package com.peterchaschke.weather;

public class UserAuthorizationException extends RuntimeException {
	
	private static final long serialVersionUID = 3270778785679339443L;
	
	private FieldError fieldError;
	
	public UserAuthorizationException(String field, Object rejectedValue, String message) {
		this.fieldError = new FieldError(field, rejectedValue, message);
	}
	
	public String getMessage() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("User Authorization Error.\n");
		sb.append("Field: ");
		sb.append(fieldError.getFieldName());
		sb.append("\n");
		sb.append("Rejected Value: ");
		sb.append(fieldError.getRejectedValue());
		sb.append("\n");
		sb.append("Message: ");
		sb.append(fieldError.getMessage());
		sb.append("\n");
		
		return sb.toString();
	}
}
