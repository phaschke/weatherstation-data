package com.peterchaschke.weather.truncation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TruncationEntryRepository extends JpaRepository<TruncationEntry, Long> {
	
	public List<TruncationEntry> findAllByStationIdAndSiteId(Long stationId, Long siteId);
}

